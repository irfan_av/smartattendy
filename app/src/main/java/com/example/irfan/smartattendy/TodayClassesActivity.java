package com.example.irfan.smartattendy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.models.Subject;
import com.example.irfan.smartattendy.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;

public class TodayClassesActivity extends AppCompatActivity {

    @BindView(R.id.lv_classes) ListView lvClasses;
    List<Subject> subjects;
    User currUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today_classes);
        ButterKnife.bind(this);

        currUser = Paper.book().read(Constants.CURR_USER_KEY);

    }
}
