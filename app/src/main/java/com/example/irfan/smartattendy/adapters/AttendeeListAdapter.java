package com.example.irfan.smartattendy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.irfan.smartattendy.R;
import com.example.irfan.smartattendy.StudentViewActivity;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Attendee;
import com.example.irfan.smartattendy.models.User;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import spencerstudios.com.bungeelib.Bungee;

/**
 * Created by irfan on 11/24/18.
 */

public class AttendeeListAdapter extends ArrayAdapter {

    private Context context;
    private List<User> users;
    private String courseId;
    private long year;

    public AttendeeListAdapter(@NonNull Context context, List<User> users, String courseId, long year) {
        super(context, R.layout.attendee_list_item, users);

        this.context = context;
        this.users = users;
        this.courseId = courseId;
        this.year = year;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = View.inflate(context, R.layout.attendee_list_item, null);

        CircleImageView civUserAvatar = view.findViewById(R.id.civ_user_avatar);
        TextView tvUserName = view.findViewById(R.id.tv_user_name);

        User user = users.get(position);

        tvUserName.setText(user.name);

        if (user.photoUrl != null) {
            Glide.with(context.getApplicationContext()).load(user.photoUrl).into(civUserAvatar);

        } else {
            civUserAvatar.setImageDrawable(Helper.setAvatar(user.gender, user.roleId, context.getApplicationContext()));
        }

        view.setOnClickListener(view1 -> {
            Intent intent = new Intent(context.getApplicationContext(), StudentViewActivity.class);
            intent.putExtra("STUDENT", user);
            intent.putExtra("COURSEID", courseId);
            intent.putExtra("YEAR", year);
            context.startActivity(intent);
            Bungee.slideLeft(context);
        });

        return view;
    }
}
