//package com.example.irfan.smartattendy;
//
//import android.Manifest;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.net.Uri;
//import android.os.Build;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.Toast;
//
//import com.google.zxing.Result;
//
//import me.dm7.barcodescanner.zxing.ZXingScannerView;
//
//public class StudentMainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
//
//    private static final int REQUEST_CAMERA = 1;
//    private ZXingScannerView scannerView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        scannerView = new ZXingScannerView(this);
//
//        setContentView(R.layout.activity_student_main);
//        Button button = findViewById(R.id.btn_scan);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setContentView(scannerView);
//            }
//        });
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkPermission()) {
//
//                Toast.makeText(StudentMainActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
//
//            } else {
//                requestPermission();
//            }
//        }
//    }
//
//    private boolean checkPermission() {
//        return (ContextCompat.checkSelfPermission(StudentMainActivity.this, Manifest.permission.CAMERA)
//                        == PackageManager.PERMISSION_GRANTED);
//    }
//
//    private void requestPermission() {
//        ActivityCompat.requestPermissions(this, new String[]{CAMERA_SERVICE}, REQUEST_CAMERA);
//    }
//
//    public void onRequestPermissionResult(int requestCode, String permission[], int grandResults[]) {
//        switch (requestCode) {
//            case REQUEST_CAMERA:
//                if (grandResults.length > 0) {
//                    boolean cameraAccepted = grandResults[0] == PackageManager.PERMISSION_GRANTED;
//                    if (cameraAccepted) {
//                        Toast.makeText(StudentMainActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
//
//                    } else {
//                        Toast.makeText(StudentMainActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                            if (shouldShowRequestPermissionRationale(CAMERA_SERVICE)) {
//                                    displayAlertMessage("You need to allow access for both permissions",
//                                            new DialogInterface.OnClickListener() {
//                                                @Override
//                                                public void onClick(DialogInterface dialogInterface, int i) {
//                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                                        requestPermissions(new String[]{CAMERA_SERVICE}, REQUEST_CAMERA);
//                                                    }
//                                                }
//                                            });
//                                    return;
//                            }
//                        }
//
//                    }
//                }
//
//                break;
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (checkPermission()) {
//            if (scannerView == null) {
//                scannerView = new ZXingScannerView(this);
//                setContentView(scannerView);
//            }
//            scannerView.setResultHandler(this);
//            scannerView.startCamera();
//        } else {
//            requestPermission();
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        scannerView.stopCamera();
//    }
//
//    public void displayAlertMessage(String message, DialogInterface.OnClickListener listener) {
//        new AlertDialog.Builder(StudentMainActivity.this)
//                .setMessage(message)
//                .setPositiveButton("OK", listener)
//                .setNegativeButton("Cancel", null)
//                .create()
//                .show();
//    }
//
//
//    @Override
//    public void handleResult(Result result) {
//
//        final String scanResult = result.getText();
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Scan Result");
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                scannerView.resumeCameraPreview(StudentMainActivity.this);
//            }
//        });
//        builder.setNeutralButton("Visit", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(scanResult));
//                startActivity(intent);
//            }
//        });
//        builder.setMessage(scanResult);
//        AlertDialog alertDialog = builder.create();
//        alertDialog.show();
//    }
//}
