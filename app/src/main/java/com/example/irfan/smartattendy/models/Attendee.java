package com.example.irfan.smartattendy.models;

/**
 * Created by irfan on 11/24/18.
 */

public class Attendee {

   public String name;
   public long date;
   public String time;
   public String subject;
   public String userUID;
}
