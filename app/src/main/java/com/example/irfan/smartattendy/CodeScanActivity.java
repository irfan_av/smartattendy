package com.example.irfan.smartattendy;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ErrorCallback;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Attendee;
import com.example.irfan.smartattendy.models.ClassInfo;
import com.example.irfan.smartattendy.models.Course;
import com.example.irfan.smartattendy.models.Subject;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.zxing.Result;

import java.io.Serializable;
import java.util.Calendar;

import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class CodeScanActivity extends AppCompatActivity {

    private static final int RC_PERMISSION = 10;
    private CodeScanner scanner;
    private boolean isPermissionGranted;
    private boolean isThisCourseAvailable;

    DatabaseReference dbRef;
    User currUser;
    Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_scan);

        dbRef = FirebaseDatabase.getInstance().getReference();
        currUser = Paper.book().read(Constants.CURR_USER_KEY);

        CodeScannerView scannerView = findViewById(R.id.scanner);
        scanner = new CodeScanner(this, scannerView);

        scanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(Result result) {
                CodeScanActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Gson gson = new Gson();
                        ClassInfo info = gson.fromJson(result.getText(), ClassInfo.class);

                        String courseId = Helper.getCourseId(CodeScanActivity.this, info.subject);

                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(info.dateTime);
                        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                        int month = calendar.get(Calendar.MONTH);
                        String monthNameWithDate = Helper.getMonthName(month) + "_" + dayOfMonth;

                        if (info.semesterID.equals(Helper.getSemesterId(CodeScanActivity.this, currUser.semester))) {
                            dbRef.child("attendance")
                                    .child(String.valueOf(calendar.get(Calendar.YEAR)))
                                    .child(monthNameWithDate)
                                    .child(info.instructorUID)
                                    .child(courseId)
                                    .child("attendees")
                                    .child(currUser.id)
                                    .setValue(true);

                            Intent intent = new Intent(CodeScanActivity.this, AttendeeActivity.class);
                            intent.putExtra("CLASSINFO", info);
                            intent.putExtra("TIMESTAMP", result.getTimestamp());
                            intent.putExtra("CLASSTIME", info.dateTime);
                            startActivity(intent);
                            finish();
                            Bungee.slideLeft(CodeScanActivity.this);


                        } else {
                            startActivity(new Intent(CodeScanActivity.this, CannotAttendClassActivity.class));
                            finish();
                            Bungee.slideLeft(CodeScanActivity.this);
                        }

                    }
                });
            }
        });

        scanner.setErrorCallback(new ErrorCallback() {
            @Override
            public void onError(final Exception error) {
                CodeScanActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CodeScanActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanner.startPreview();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                isPermissionGranted = false;
                requestPermissions(new String[] {Manifest.permission.CAMERA}, RC_PERMISSION);

            } else {
                isPermissionGranted = true;
            }

        } else {
            isPermissionGranted = true;
        }
    }

    public boolean isAbleToAttendClass(String courseID) {
        String semesterID = Helper.getSemesterId(this, currUser.semester);
        dbRef.child("programs")
                .child("BSCS")
                .child(semesterID)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Course course = snapshot.getValue(Course.class);
                    if (course != null)
                        if (courseID.equals(course.id)) {
                            isThisCourseAvailable = true;

                        } else {
                            isThisCourseAvailable = false;
                        }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
       return isThisCourseAvailable;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isPermissionGranted = true;
                scanner.startPreview();
            } else {
                isPermissionGranted = false;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPermissionGranted) {
            scanner.startPreview();
        }
    }

    @Override
    protected void onPause() {
        scanner.releaseResources();
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
