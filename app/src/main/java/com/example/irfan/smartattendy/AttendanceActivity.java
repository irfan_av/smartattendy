package com.example.irfan.smartattendy;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.irfan.smartattendy.adapters.AttendanceListAdapter;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.AttendanceMonth;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class AttendanceActivity extends AppCompatActivity {

//    @BindView(R.id.sp_month) Spinner spMonth;
    @BindView(R.id.tv_show_date) TextView tvShowDate;
    @BindView(R.id.tv_no_record) TextView textView;

    User currUser;
    List<AttendanceMonth> months;
    String monthName;
    Calendar today = Calendar.getInstance();

    AttendanceListAdapter adapter;

    DatabaseReference dbRef;
    DatabaseReference yearRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        ButterKnife.bind(this);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle("Attendance");
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.toolbar_drawable));
        }

        dbRef = FirebaseDatabase.getInstance().getReference();
        yearRef = FirebaseDatabase.getInstance().getReference();
        currUser = Paper.book().read(Constants.CURR_USER_KEY);
        months = new ArrayList<>();

        adapter = new AttendanceListAdapter(this, months);
        ListView listView = findViewById(R.id.lv_attendance);
        listView.setAdapter(adapter);

        if (adapter.getCount() == 0) {
            textView.setVisibility(View.VISIBLE);

        } else {
            textView.setVisibility(View.GONE);
        }
//        dbRef.child("attendance").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                months.clear();
//                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
//                    String month = snapshot.getKey();
//                    if (month != null) {
//                        monthName = month.substring(0, Math.min(month.length(), 3));
//                        String date = month.substring(month.length() - 2);
//
//                        AttendanceMonth m = new AttendanceMonth();
//                        m.name = getMonth(monthName);
//                        m.date = date;
//                        months.add(m);
//
//                    }
//
//                }
//                adapter.notifyDataSetChanged();
////                setSpItem(spMonth, getMonth(monthName));
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        Calendar calendar = Calendar.getInstance();
        showData(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
//        int year = calendar.get(Calendar.YEAR);
//        setSpItem(spMonth, String.valueOf(calendar.get(Calendar.MONTH)));

//        spYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (i == 0) {
//                    yearRef =  dbRef.child("attendance").child("2019");
//
//                } else if (i == 1) {
//                    yearRef =  dbRef.child("attendance").child("2018");
//
//                } else if (i == 2) {
//                    yearRef = dbRef.child("attendance").child("2017");
//
//                } else if (i == 3) {
//                    yearRef =  dbRef.child("attendance").child("2016");
//
//                } else if (i == 4) {
//                    yearRef = dbRef.child("attendance").child("2015");
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

//        spMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (i == 0) {
//                    showData("Jan");
//
//                } else if (i == 1) {
//                    showData("Feb");
//
//                } else if (i == 2) {
//                    showData("Mar");
//
//                } else if (i == 3) {
//                    showData("Apr");
//
//                } else if (i == 4) {
//                    showData("May");
//
//                } else if (i == 5) {
//                    showData("Jun");
//
//                } else if (i == 6) {
//                    showData("Jul");
//
//                } else if (i == 7) {
//                    showData("Aug");
//
//                } else if (i == 8) {
//                    showData("Sep");
//
//                } else if (i == 9) {
//                    showData("Oct");
//
//                } else if (i == 10) {
//                    showData("Nov");
//
//                } else if (i == 11) {
//                    showData("Dec");
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
    }

    public void showData(int m, int year) {

        adapter.clear();
        String selectedMonth = Helper.getMonthName(m);
        String selectedYear = String.valueOf(year);

        Helper.showProgress(this, true);
        dbRef.child("attendance").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String year = snapshot.getKey();
                    if (year != null) {
                        if (selectedYear.equals(year)) {
                            dbRef.child("attendance")
                                    .child(selectedYear)
                                    .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    months.clear();
                                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                        String month = snapshot.getKey();
                                        if (month != null) {
                                            Helper.removeProgress(AttendanceActivity.this);

                                            monthName = month.substring(0, Math.min(month.length(), 3));

                                            String selectedMon = selectedMonth.toLowerCase();
                                            String dataMon = monthName.toLowerCase();

                                            if (dataMon.equals(selectedMon)) {
                                                String date = month.substring(month.length() - 2);

                                                AttendanceMonth m = new AttendanceMonth();
                                                m.name = getMonth(monthName);
                                                m.date = checkUnderscore(date);
                                                m.year = selectedYear;
                                                months.add(m);

                                            }
                                        }
                                        Helper.removeProgress(AttendanceActivity.this);

                                    }
                                    adapter.notifyDataSetChanged();

                                    if (adapter.getCount() == 0) {

                                        textView.setVisibility(View.VISIBLE);

                                    } else {
                                        textView.setVisibility(View.GONE);
                                    }

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                    Helper.removeProgress(AttendanceActivity.this);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    public String checkUnderscore(String date) {
        if (date.substring(0, Math.min(date.length(), 1)).equals("_")) {
            return date.substring(date.length() - 1);

        } else {
            return date;
        }
    }


    public void setSpItem(Spinner spinner, String value) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).equals(value)) {
                spinner.setSelection(i);
                break;
            }
        }
    }


    public String getMonth(String m) {
        switch (m) {
            case "Jan":
                return getResources().getString(R.string.m_jan);

            case "Feb":
                return getResources().getString(R.string.m_feb);

            case "Mar":
                return getResources().getString(R.string.m_mar);

            case "Apr":
                return getResources().getString(R.string.m_apr);

            case "May":
                return getResources().getString(R.string.m_may);

            case "Jun":
                return getResources().getString(R.string.m_jun);

            case "Jul":
                return getResources().getString(R.string.m_jul);

            case "Aug":
                return getResources().getString(R.string.m_aug);

            case "Sep":
                return getResources().getString(R.string.m_sep);

            case "Oct":
                return getResources().getString(R.string.m_oct);

            case "Nov":
                return getResources().getString(R.string.m_nov);

            default:
                return getResources().getString(R.string.m_dec);
        }

    }


    public void showDateFilterDialog() {
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(AttendanceActivity.this,
                new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                showData(selectedMonth, selectedYear);
            }

        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.setActivatedMonth(today.get(Calendar.MONTH))
                .setMinYear(2015)
                .setActivatedYear(today.get(Calendar.YEAR))
                .setMaxYear(2025)
                .build().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_attendance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_attendance_item_month:
                showDateFilterDialog();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
