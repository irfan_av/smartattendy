package com.example.irfan.smartattendy;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

public class SignUpActivity extends AppCompatActivity {

//    @BindView(R.id.sp_user_type) Spinner spUserType;
    @BindView(R.id.text_input_name) TextInputEditText etName;
//    @BindView(R.id.text_input_last_name) TextInputEditText etLastName;
    @BindView(R.id.text_input_email) TextInputEditText etEmail;
    @BindView(R.id.text_input_phone_number) TextInputEditText etPhoneNumber;
    @BindView(R.id.text_input_password) TextInputEditText etPassword;
//    @BindView(R.id.text_input_confirm_password) TextInputEditText etConfirmPassword;
    @BindView(R.id.btn_sign_up) Button btnSignUp;

    FirebaseAuth auth;
    DatabaseReference dbRef;
    int userRole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        auth = FirebaseAuth.getInstance();
        dbRef = FirebaseDatabase.getInstance().getReference();

        userRole = getIntent().getIntExtra("ROLE", 10);
        Log.d("ROLE", String.valueOf(userRole));
    }

    @OnClick(R.id.btn_sign_up)
    public void setSignUp() {
        boolean flag = true;

        if (etName.getText().toString().isEmpty()) {
            etName.setError("Name Required!");
            flag = false;

        } else {
            etName.setError(null);
        }

//        if (etLastName.getText().toString().isEmpty()) {
//            etLastName.setError("Phone Number Required!");
//            flag = false;
//
//        } else {
//            etLastName.setError(null);
//        }

        if (etEmail.getText().toString().isEmpty()) {
            etEmail.setError("Email Required!");
            flag = false;

        } else {
            etEmail.setError(null);
        }

        if (etPhoneNumber.getText().toString().isEmpty()) {
            etPhoneNumber.setError("Phone Number Required!");
            flag = false;

        } else {
            etPhoneNumber.setError(null);
        }

        if (etPassword.getText().toString().isEmpty()) {
            etPassword.setError("Password Required!");
            flag = false;

        } else {
            etPassword.setError(null);
        }

//        if (etConfirmPassword.getText().toString().isEmpty()) {
//            etConfirmPassword.setError("Re-enter Password!");
//            flag = false;

//        } else if (!etConfirmPassword.getText().toString().equals(etPassword.getText().toString())) {
//            etConfirmPassword.setError("Incorrect Password");
//
//        } else  {
//            etConfirmPassword.setError(null);
//        }

        if (!flag) {
            return;
        }

        Helper.showProgress(this, true);
        auth.createUserWithEmailAndPassword(etEmail.getText().toString(), etPassword.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        Helper.removeProgress(SignUpActivity.this);
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            if (firebaseUser != null) {
                                firebaseUser.sendEmailVerification();
                            }
                            String id = firebaseUser.getUid();

                            User user = new User();
                            user.id = id;
                            user.roleId = userRole;
                            user.name = etName.getText().toString().trim();
//                            user.lastname = etLastName.getText().toString().trim();
                            user.email = etEmail.getText().toString().trim();
                            user.phone = etPhoneNumber.getText().toString().trim();

                            dbRef.child("users").child(id).setValue(user);
                            startActivity(new Intent(SignUpActivity.this, EmailVerificationSentActivity.class));
                            Bungee.slideLeft(SignUpActivity.this);
                            finish();

                        } else {
                            Helper.removeProgress(SignUpActivity.this);
                            Log.d("TAG", task.getException().getMessage());
                            Toast.makeText(SignUpActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Bungee.slideDown(this);
    }
}
