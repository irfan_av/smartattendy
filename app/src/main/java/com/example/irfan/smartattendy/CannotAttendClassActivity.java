package com.example.irfan.smartattendy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import spencerstudios.com.bungeelib.Bungee;

public class CannotAttendClassActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cannot_attend_class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Bungee.slideRight(this);
    }
}
