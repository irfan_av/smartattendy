package com.example.irfan.smartattendy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.irfan.smartattendy.R;
import com.example.irfan.smartattendy.SubjectListActivity;
import com.example.irfan.smartattendy.models.AttendanceMonth;

import java.util.List;

import spencerstudios.com.bungeelib.Bungee;

/**
 * Created by irfan on 12/18/18.
 */

public class AttendanceListAdapter extends ArrayAdapter {

    Context context;
    List<AttendanceMonth> months;

    public AttendanceListAdapter(@NonNull Context context, List<AttendanceMonth> months) {
        super(context, R.layout.attendance_month_list_item, months);
        this.context = context;
        this.months = months;
    }

    @Override
    public int getCount() {
        return months.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = View.inflate(context, R.layout.attendance_month_list_item, null);

        TextView textView = view.findViewById(R.id.tv_month);
        TextView tvYear = view.findViewById(R.id.tv_year);

        AttendanceMonth month = months.get(position);
        textView.setText(month.name + " " + month.date );

        if (month.year != null) {
            tvYear.setText(month.year);
        } else {
            tvYear.setVisibility(View.GONE);
        }

        String monthName = month.name.substring(0, Math.min(month.name.length(), 3));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context.getApplicationContext(), SubjectListActivity.class);
                intent.putExtra("DATE", monthName +"_"+ month.date);
                intent.putExtra("YEAR", month.year);
                context.startActivity(intent);
                Bungee.slideLeft(context);
            }
        });

        return view;
    }
}
