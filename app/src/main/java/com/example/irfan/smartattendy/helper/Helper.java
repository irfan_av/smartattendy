package com.example.irfan.smartattendy.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.irfan.smartattendy.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by irfan on 12/8/18.
 */

public class Helper {

    public static void closeKeyboard(Activity context) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public static void showProgress(AppCompatActivity activity, boolean isCancellable) {
        final RelativeLayout rl = activity.findViewById(R.id.spinner_view);
        if (rl == null)
            return;

        if (!isNetworkAvailable(activity)) {
            Toast.makeText(activity, activity.getString(R.string.connect_internet), Toast.LENGTH_SHORT).show();
            return;
        }

        rl.setVisibility(View.VISIBLE);
        rl.setClickable(!isCancellable);

        if (isCancellable) {
            rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rl.setVisibility(View.GONE);
                }
            });
        }
    }


    public static void removeProgress(Activity activity) {
        RelativeLayout rl = activity.findViewById(R.id.spinner_view);
        if (rl == null)
            return;

        rl.setVisibility(View.GONE);
    }


    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager =
                ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() !=
                null && connectivityManager.getActiveNetworkInfo().isConnected();
    }


    public static String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.USER_FRIENDLY_DATE_FORMAT);
        return sdf.format(date);
    }

    public static Date parseDate(String dateStr) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.USER_FRIENDLY_DATE_FORMAT);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        }
        catch (Exception e) {}
        return date;
    }

    public static String getMonthName(int i) {
        switch (i) {
            case 0:
                return "Jan";
            case 1:
                return "Feb";
            case 2:
                return "Mar";
            case 3:
                return "Apr";
            case 4:
                return "May";
            case 5:
                return "Jun";
            case 6:
                return "Jul";
            case 7:
                return "Aug";
            case 8:
                return "Sep";
            case 9:
                return "Oct";
            case 10:
                return "Nov";
            case 11:
                return "Dec";
            default:
                return "Jan";
        }
    }

    public static String getWeekName(int i) {
        switch (i) {
            case 1:
                return "Sun";
            case 2:
                return "Mon";
            case 3:
                return "Tue";
            case 4:
                return "Wed";
            case 5:
                return "Thu";
            case 6:
                return "Fri";
            case 7:
                return "Sat";
            default:
                return "Sun";
        }
    }

    public static String getSemesterId(Activity activity, String semester) {

        if (semester.equals(activity.getResources().getString(R.string.first_semester))) {
            return activity.getResources().getString(R.string.code_first_semester);
        }

        if (semester.equals(activity.getResources().getString(R.string.sec_semester))) {
            return activity.getResources().getString(R.string.code_sec_semester);
        }

        if (semester.equals(activity.getResources().getString(R.string.third_semester))) {
            return activity.getResources().getString(R.string.code_thrd_semester);
        }

        if (semester.equals(activity.getResources().getString(R.string.forth_semester))) {
            return activity.getResources().getString(R.string.code_forth_semester);
        }

        if (semester.equals(activity.getResources().getString(R.string.fifth_semester))) {
            return activity.getResources().getString(R.string.code_fifth_semester);
        }

        if (semester.equals(activity.getResources().getString(R.string.sixth_semester))) {
            return activity.getResources().getString(R.string.code_sixth_semester);
        }

        if (semester.equals(activity.getResources().getString(R.string.seventh_semester))) {
            return activity.getResources().getString(R.string.code_seventh_semester);
        }

        if (semester.equals(activity.getResources().getString(R.string.eighth_semester))) {
            return activity.getResources().getString(R.string.code_eighth_semester);
        }
        return "Not_available";
    }

    public static String getCourseName(Activity activity, String courseId) {

        if (courseId.equals(activity.getResources().getString(R.string.course_id_eng_I))) {
            return activity.getResources().getString(R.string.eng_1);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_basic_elec))) {
            return activity.getResources().getString(R.string.subject_basic_elec);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_calculus))) {
            return activity.getResources().getString(R.string.subject_Calculus);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_prog_fundamentals))) {
            return activity.getResources().getString(R.string.subject_program_fundamentals);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_intro_to_comp))) {
            return activity.getResources().getString(R.string.subject_intro_to_comp);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_islamic))) {
            return activity.getResources().getString(R.string.subject_islamic_st);
        }

        /*Second semester courses*/
        if (courseId.equals(activity.getResources().getString(R.string.course_id_eng_II))) {
            return activity.getResources().getString(R.string.subj_eng_II);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_oop))) {
            return activity.getResources().getString(R.string.subj_oop);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_pak_studies))) {
            return activity.getResources().getString(R.string.subj_pak_studies);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_dld))) {
            return activity.getResources().getString(R.string.subj_dld);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_dis_maths))) {
            return activity.getResources().getString(R.string.subj_dis_maths);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_prob_stats))) {
            return activity.getResources().getString(R.string.subj_probability_stats);
        }

        /*Third semester*/
        if (courseId.equals(activity.getResources().getString(R.string.course_id_urdu))) {
            return activity.getResources().getString(R.string.urdu);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_diff_eq))) {
            return activity.getResources().getString(R.string.diff_equation);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_assembly_lang))) {
            return activity.getResources().getString(R.string.assembly_lang);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_visual_prog))) {
            return activity.getResources().getString(R.string.visual_prog);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_data_structure))) {
            return activity.getResources().getString(R.string.data_structure);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_dbms))) {
            return activity.getResources().getString(R.string.dbms);
        }

        /*Forth semester*/
        if (courseId.equals(activity.getResources().getString(R.string.course_id_adbms))) {
            return activity.getResources().getString(R.string.adbms);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_soft_eng))) {
            return activity.getResources().getString(R.string.software_eng);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_data_com))) {
            return activity.getResources().getString(R.string.data_com);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_comp_architect))) {
            return activity.getResources().getString(R.string.comp_architect);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_linear_algebra))) {
            return activity.getResources().getString(R.string.linear_algebra);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_operating_sys))) {
            return activity.getResources().getString(R.string.operating_sys);
        }

        /*fifth semester*/
        if (courseId.equals(activity.getResources().getString(R.string.course_id_ai))) {
            return activity.getResources().getString(R.string.artificial_intelligence);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_theory_of_auto))) {
            return activity.getResources().getString(R.string.theory_of_automata);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_web_prog))) {
            return activity.getResources().getString(R.string.web_prog);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_oo_soft_eng))) {
            return activity.getResources().getString(R.string.oo_soft_eng);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_algo_analysis))) {
            return activity.getResources().getString(R.string.algo_analysis);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_comp_com_network))) {
            return activity.getResources().getString(R.string.comp_com_network);
        }

        /*sixth semester*/
        if (courseId.equals(activity.getResources().getString(R.string.course_id_data_mining))) {
            return activity.getResources().getString(R.string.data_mining);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_num_analysis))) {
            return activity.getResources().getString(R.string.numerical_analysis);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_comp_graphic))) {
            return activity.getResources().getString(R.string.comp_graphics);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_soft_pro_manage))) {
            return activity.getResources().getString(R.string.software_project_manag);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_compiler_cons))) {
            return activity.getResources().getString(R.string.compiler_construction);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_digital_image_process))) {
            return activity.getResources().getString(R.string.digital_image_process);
        }

        /*seventh semester*/
        if (courseId.equals(activity.getResources().getString(R.string.course_id_machine_learn))) {
            return activity.getResources().getString(R.string.machine_learning);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_cloud_comp))) {
            return activity.getResources().getString(R.string.cloud_computing);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_network_security))) {
            return activity.getResources().getString(R.string.network_security);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_human_comp_interac))) {
            return activity.getResources().getString(R.string.human_comp_interaction);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_comp_vision))) {
            return activity.getResources().getString(R.string.comp_vision);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_fyp_I))) {
            return activity.getResources().getString(R.string.fyp_I);
        }

        /*eighth semester*/
        if (courseId.equals(activity.getResources().getString(R.string.course_id_pattern_recog))) {
            return activity.getResources().getString(R.string.pattern_recognition);
        }

        if (courseId.equals(activity.getResources().getString(R.string.course_id_fyp_II))) {
            return activity.getResources().getString(R.string.fyp_II);
        }

        return "Not Available";
    }

    public static String getCourseId(Activity activity, String courseName) {
        if (courseName.equals(activity.getResources().getString(R.string.eng_1))) {
            return activity.getResources().getString(R.string.course_id_eng_I);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subject_basic_elec))) {
            return activity.getResources().getString(R.string.course_id_basic_elec);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subject_Calculus))) {
            return activity.getResources().getString(R.string.course_id_calculus);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subject_program_fundamentals))) {
            return activity.getResources().getString(R.string.course_id_prog_fundamentals);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subject_intro_to_comp))) {
            return activity.getResources().getString(R.string.course_id_intro_to_comp);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subject_islamic_st))) {
            return activity.getResources().getString(R.string.course_id_islamic);
        }

        /*Second semester courses*/
        if (courseName.equals(activity.getResources().getString(R.string.subj_eng_II))) {
            return activity.getResources().getString(R.string.course_id_eng_II);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subj_oop))) {
            return activity.getResources().getString(R.string.course_id_oop);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subj_pak_studies))) {
            return activity.getResources().getString(R.string.course_id_pak_studies);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subj_dld))) {
            return activity.getResources().getString(R.string.course_id_dld);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subj_dis_maths))) {
            return activity.getResources().getString(R.string.course_id_dis_maths);
        }

        if (courseName.equals(activity.getResources().getString(R.string.subj_probability_stats))) {
            return activity.getResources().getString(R.string.course_id_prob_stats);
        }

        /*Third semester*/
        if (courseName.equals(activity.getResources().getString(R.string.urdu))) {
            return activity.getResources().getString(R.string.course_id_urdu);
        }

        if (courseName.equals(activity.getResources().getString(R.string.diff_equation))) {
            return activity.getResources().getString(R.string.course_id_diff_eq);
        }

        if (courseName.equals(activity.getResources().getString(R.string.assembly_lang))) {
            return activity.getResources().getString(R.string.course_id_assembly_lang);
        }

        if (courseName.equals(activity.getResources().getString(R.string.visual_prog))) {
            return activity.getResources().getString(R.string.course_id_visual_prog);
        }

        if (courseName.equals(activity.getResources().getString(R.string.data_structure))) {
            return activity.getResources().getString(R.string.course_id_data_structure);
        }

        if (courseName.equals(activity.getResources().getString(R.string.dbms))) {
            return activity.getResources().getString(R.string.course_id_dbms);
        }

        /*Forth semester*/
        if (courseName.equals(activity.getResources().getString(R.string.adbms))) {
            return activity.getResources().getString(R.string.course_id_adbms);
        }

        if (courseName.equals(activity.getResources().getString(R.string.software_eng))) {
            return activity.getResources().getString(R.string.course_id_oo_soft_eng);
        }

        if (courseName.equals(activity.getResources().getString(R.string.data_com))) {
            return activity.getResources().getString(R.string.course_id_data_com);
        }

        if (courseName.equals(activity.getResources().getString(R.string.comp_architect))) {
            return activity.getResources().getString(R.string.course_id_comp_architect);
        }

        if (courseName.equals(activity.getResources().getString(R.string.linear_algebra))) {
            return activity.getResources().getString(R.string.course_id_linear_algebra);
        }

        if (courseName.equals(activity.getResources().getString(R.string.operating_sys))) {
            return activity.getResources().getString(R.string.course_id_operating_sys);
        }

        /*fifth semester*/
        if (courseName.equals(activity.getResources().getString(R.string.artificial_intelligence))) {
            return activity.getResources().getString(R.string.course_id_ai);
        }

        if (courseName.equals(activity.getResources().getString(R.string.theory_of_automata))) {
            return activity.getResources().getString(R.string.course_id_theory_of_auto);
        }

        if (courseName.equals(activity.getResources().getString(R.string.web_prog))) {
            return activity.getResources().getString(R.string.course_id_web_prog);
        }

        if (courseName.equals(activity.getResources().getString(R.string.oo_soft_eng))) {
            return activity.getResources().getString(R.string.course_id_oo_soft_eng);
        }

        if (courseName.equals(activity.getResources().getString(R.string.algo_analysis))) {
            return activity.getResources().getString(R.string.course_id_algo_analysis);
        }

        if (courseName.equals(activity.getResources().getString(R.string.comp_com_network))) {
            return activity.getResources().getString(R.string.course_id_comp_com_network);
        }

        /*sixth semester*/
        if (courseName.equals(activity.getResources().getString(R.string.data_mining))) {
            return activity.getResources().getString(R.string.course_id_data_mining);
        }

        if (courseName.equals(activity.getResources().getString(R.string.numerical_analysis))) {
            return activity.getResources().getString(R.string.course_id_num_analysis);
        }

        if (courseName.equals(activity.getResources().getString(R.string.comp_graphics))) {
            return activity.getResources().getString(R.string.course_id_comp_graphic);
        }

        if (courseName.equals(activity.getResources().getString(R.string.software_project_manag))) {
            return activity.getResources().getString(R.string.course_id_soft_pro_manage);
        }

        if (courseName.equals(activity.getResources().getString(R.string.compiler_construction))) {
            return activity.getResources().getString(R.string.course_id_compiler_cons);
        }

        if (courseName.equals(activity.getResources().getString(R.string.digital_image_process))) {
            return activity.getResources().getString(R.string.course_id_digital_image_process);
        }

        /*seventh semester*/
        if (courseName.equals(activity.getResources().getString(R.string.machine_learning))) {
            return activity.getResources().getString(R.string.course_id_machine_learn);
        }

        if (courseName.equals(activity.getResources().getString(R.string.cloud_computing))) {
            return activity.getResources().getString(R.string.course_id_cloud_comp);
        }

        if (courseName.equals(activity.getResources().getString(R.string.network_security))) {
            return activity.getResources().getString(R.string.course_id_network_security);
        }

        if (courseName.equals(activity.getResources().getString(R.string.human_comp_interaction))) {
            return activity.getResources().getString(R.string.course_id_human_comp_interac);
        }

        if (courseName.equals(activity.getResources().getString(R.string.comp_vision))) {
            return activity.getResources().getString(R.string.course_id_comp_vision);
        }

        if (courseName.equals(activity.getResources().getString(R.string.fyp_I))) {
            return activity.getResources().getString(R.string.course_id_fyp_I);
        }

        /*eighth semester*/
        if (courseName.equals(activity.getResources().getString(R.string.pattern_recognition))) {
            return activity.getResources().getString(R.string.course_id_pattern_recog);
        }

        if (courseName.equals(activity.getResources().getString(R.string.fyp_II))) {
            return activity.getResources().getString(R.string.course_id_fyp_II);
        }

        return "Not_available";
    }

    public static Drawable setAvatar(String gender, int roleId, Context activity) {
        if (roleId == 0 && gender != null) {
            if (gender.equals("Male")) {
                return activity.getResources().getDrawable(R.drawable.boy);
            }

            else if (gender.equals("Female")) {
                return activity.getResources().getDrawable(R.drawable.girl);

            }
        }

        else if (roleId == 1 && gender != null) {
            if (gender.equals("Male"))
                return activity.getResources().getDrawable(R.drawable.prof_avatar_male);

            else if (gender.equals("Female")){
                return activity.getResources().getDrawable(R.drawable.prof_avatar_female);

            }
        }

        return activity.getResources().getDrawable(R.drawable.avatar);

    }
}
