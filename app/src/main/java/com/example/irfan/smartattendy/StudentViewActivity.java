package com.example.irfan.smartattendy;

import android.drm.DrmStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Subject;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class StudentViewActivity extends AppCompatActivity {

    @BindView(R.id.iv_back_arrow) ImageView ivBackArrow;
    @BindView(R.id.civ_user_avatar) CircleImageView civUserPhoto;
    @BindView(R.id.tv_user_name) TextView tvUserName;
    @BindView(R.id.tv_user_semester) TextView tvUserSemester;
    @BindView(R.id.tv_user_class_roll_no) TextView tvClassRollNumber;
    @BindView(R.id.tv_admit_card_no) TextView tvAdmitCardNo;
    @BindView(R.id.tv_enrollment_no) TextView tvEnrollmentNo;
    @BindView(R.id.tv_user_email) TextView tvUserEmail;

    @BindView(R.id.tv_attendance) TextView tvAttendance;
    @BindView(R.id.tv_presence) TextView tvPresence;
    @BindView(R.id.tv_absence) TextView tvAbsence;

    DatabaseReference dbRef;
    User currUser;

    int classes = 0;
    int presence = 0;
    int absence = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_view);
        ButterKnife.bind(this);

        currUser = Paper.book().read(Constants.CURR_USER_KEY);
        User user = (User) getIntent().getSerializableExtra("STUDENT");
        String courseId = getIntent().getStringExtra("COURSEID");
//        String year = getIntent().getStringExtra("YEAR");

        Calendar calendar = Calendar.getInstance();
        long year = getIntent().getLongExtra("YEAR", calendar.get(Calendar.YEAR));

        dbRef = FirebaseDatabase.getInstance().getReference();

        tvUserName.setText(user.name);
        tvUserEmail.setText(user.email);

        if (user.photoUrl != null) {
            Glide.with(this).load(user.photoUrl).into(civUserPhoto);

        } else {
            civUserPhoto.setImageDrawable(Helper.setAvatar(user.gender, user.roleId,
                    StudentViewActivity.this));
        }

        if (user.semester != null) {
            tvUserSemester.setText(user.semester);

        } else {
            tvUserSemester.setText("N/A");
        }

        if (user.classRollNo != null) {
            tvClassRollNumber.setText(user.classRollNo);

        } else {
            tvClassRollNumber.setText("N/A");
        }

        if (user.admitCardNumber != null) {
            tvAdmitCardNo.setText(user.admitCardNumber);

        } else {
            tvAdmitCardNo.setText("N/A");
        }

        if (user.enrollmentNumber != null) {
            tvEnrollmentNo.setText(user.enrollmentNumber);

        } else {
            tvEnrollmentNo.setText("N/A");
        }

        Helper.showProgress(this, true);
        dbRef.child("attendance")
                .child(String.valueOf(year))
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String date = snapshot.getKey();

                    if (date != null) {
                        dbRef.child("attendance")
                                .child(String.valueOf(year))
                                .child(date)
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot snapshot1 : dataSnapshot.getChildren()) {
                                    String uid = snapshot1.getKey();

                                    if (currUser.id.equals(uid)) {

                                        dbRef.child("attendance")
                                                .child(String.valueOf(year))
                                                .child(date)
                                                .child(uid)
                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                                    String courseID = snap.getKey();

                                                    if (courseID != null && courseID.equals(courseId)) {
                                                        classes++;

                                                        dbRef.child("attendance")
                                                                .child(String.valueOf(year))
                                                                .child(date)
                                                                .child(uid)
                                                                .child(courseId)
                                                                .child("attendees")
                                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        for (DataSnapshot snapshot2 : dataSnapshot.getChildren()) {
                                                                            Helper.removeProgress(StudentViewActivity.this);
                                                                            String id = snapshot2.getKey();
                                                                            if (user.id.equals(id)) {
                                                                                presence++;
                                                                            }
                                                                        }

                                                                        absence = classes - presence;
                                                                        tvAttendance.setText(String.valueOf(classes));
                                                                        tvPresence.setText(String.valueOf(presence));
                                                                        tvAbsence.setText(String.valueOf(absence));

                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                    }
                                                                });
                                                    }
                                                }


                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });

                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @OnClick(R.id.iv_back_arrow)
    public void setIvBackArrow() {
        finish();
        Bungee.slideRight(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
