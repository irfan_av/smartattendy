package com.example.irfan.smartattendy;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.irfan.smartattendy.adapters.CourseListAdapter;
import com.example.irfan.smartattendy.adapters.SubjectListAdapter;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Course;
import com.example.irfan.smartattendy.models.Subject;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class MyAttendanceActivity extends AppCompatActivity {

    @BindView(R.id.lv_my_attendance) ListView lvSubjectList;

    List<Course> subjects;
    String[] courses;

    User currUser;
    DatabaseReference dbRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_attendance);
        ButterKnife.bind(this);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.toolbar_drawable));
            bar.setTitle("My Attendance");
        }

        dbRef = FirebaseDatabase.getInstance().getReference();
        currUser = Paper.book().read(Constants.CURR_USER_KEY);

        User currUser = Paper.book().read(Constants.CURR_USER_KEY);
        if (currUser.semester.equals(getResources().getString(R.string.first_semester))) {
            courses = getResources().getStringArray(R.array.semester_1_subs);

        } else if (currUser.semester.equals(getResources().getString(R.string.sec_semester))) {
            courses = getResources().getStringArray(R.array.semester_2_subs);

        } else if (currUser.semester.equals(getResources().getString(R.string.third_semester))) {
            courses = getResources().getStringArray(R.array.semester_3_subs);

        } else if (currUser.semester.equals(getResources().getString(R.string.forth_semester))) {
            courses = getResources().getStringArray(R.array.semester_4_subs);

        } else if (currUser.semester.equals(getResources().getString(R.string.fifth_semester))) {
            courses = getResources().getStringArray(R.array.semester_5_subs);

        } else if (currUser.semester.equals(getResources().getString(R.string.sixth_semester))) {
            courses = getResources().getStringArray(R.array.semester_6_subs);

        } else if (currUser.semester.equals(getResources().getString(R.string.seventh_semester))) {
            courses = getResources().getStringArray(R.array.semester_7_subs);

        } else if (currUser.semester.equals(getResources().getString(R.string.eighth_semester))) {
            courses = getResources().getStringArray(R.array.semester_8_subs);

        }


        subjects = new ArrayList<>();

//        for (String course : courses) {
//            Course subject = new Course();
//            subject.name = course;
//            subjects.add(subject);
//        }

        CourseListAdapter adapter = new CourseListAdapter(this, subjects);
        lvSubjectList.setAdapter(adapter);

        String semesterId = Helper.getSemesterId(this, currUser.semester);

        dbRef.child("programs")
                .child("BSCS")
                .child(semesterId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Course course = snapshot.getValue(Course.class);
                    subjects.add(course);
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
