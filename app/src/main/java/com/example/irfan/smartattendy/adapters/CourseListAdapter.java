package com.example.irfan.smartattendy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.irfan.smartattendy.AttendeesListActivity;
import com.example.irfan.smartattendy.R;
import com.example.irfan.smartattendy.StudentAttendanceActivity;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.models.Course;
import com.example.irfan.smartattendy.models.Subject;
import com.example.irfan.smartattendy.models.User;

import java.util.List;

import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

/**
 * Created by irfan on 12/31/18.
 */

public class CourseListAdapter extends ArrayAdapter {

    Context context;
    List<Course> subjects;
    User currUser;

    public CourseListAdapter(@NonNull Context context, List<Course> subjects) {
        super(context, R.layout.subject_list_item, subjects);

        this.context = context;
        this.subjects = subjects;
    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = View.inflate(context, R.layout.subject_list_item, null);

        currUser = Paper.book().read(Constants.CURR_USER_KEY);

        TextView textView = view.findViewById(R.id.tv_subject_name);

        Course course = subjects.get(position);
        textView.setText(course.name);

        if (currUser.roleId == 0) {
            view.setOnClickListener(view1 -> {
                Intent intent = new Intent(context.getApplicationContext(), StudentAttendanceActivity.class);
                intent.putExtra("COURSE", course);
                context.startActivity(intent);
                Bungee.slideLeft(context);
            });

        }

        return view;
    }

}
