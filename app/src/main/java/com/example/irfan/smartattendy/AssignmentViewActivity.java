package com.example.irfan.smartattendy;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Assignment;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class AssignmentViewActivity extends AppCompatActivity {

    @BindView(R.id.tv_due_date) TextView tvDueDate;
    @BindView(R.id.tv_course_name) TextView tvCourseName;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_description) TextView tvDescription;


    User currUser;
    DatabaseReference dbRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_view);
        ButterKnife.bind(this);


        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle("Assignment Detail");
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.background_toolbar));
        }

        currUser = Paper.book().read(Constants.CURR_USER_KEY);
        dbRef = FirebaseDatabase.getInstance().getReference();

        Assignment assignment = (Assignment) getIntent().getSerializableExtra("ASSIGNMENT");

        if (assignment != null) {
            tvCourseName.setText(assignment.course);
            tvTitle.setText(assignment.title);
            tvDescription.setText(assignment.description);

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(assignment.dueDate);

            int m = calendar.get(Calendar.MONTH);
            String month = Helper.getMonthName(m);
            int date = calendar.get(Calendar.DAY_OF_MONTH);

            int lastDigit = date % 10;
            Log.d("TAG", String.valueOf(lastDigit));

            tvDueDate.setText(Html.fromHtml(date + "<small><sup>"+checkLastDigitOfDate(date)+"</sup></small> " + month));
            checkDate(assignment.dueDate, tvDueDate);
        }

    }

    public void checkDate(long date, TextView textView) {
        Calendar calendar = Calendar.getInstance();

        if (date >= calendar.getTimeInMillis()) {
            textView.setTextColor(getResources().getColor(R.color.green_light));

        } else {
            textView.setTextColor(getResources().getColor(R.color.red_light));
        }

    }

    public String checkLastDigitOfDate(int date) {
        int lastDigit = date % 10;

        if (lastDigit == 1) {
            return "st";

        } else if (lastDigit == 2) {
            return "nd";

        } else if (lastDigit == 3){
            return "rd";

        } else {
            return "th";
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                Bungee.slideRight(this);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
