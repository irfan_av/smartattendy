package com.example.irfan.smartattendy;

import android.graphics.Bitmap;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.ClassInfo;
import com.example.irfan.smartattendy.models.Subject;
import com.example.irfan.smartattendy.models.User;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.stream.Stream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class CreateClassActivity extends AppCompatActivity {

    @BindView(R.id.linear_layout) LinearLayout linearLayout;
//    @BindView(R.id.tiet_subject) TextInputEditText etSubject;
//    @BindView(R.id.tiet_semester) TextInputEditText etSemester;

    @BindView(R.id.sp_semester) Spinner spSemesters;
    @BindView(R.id.sp_subjects) Spinner spSubjects;

    @BindView(R.id.tiet_duration) TextInputEditText etDuration;
    @BindView(R.id.btn_generate_qr_code) Button btnGenerate;
    @BindView(R.id.relative_layout) RelativeLayout relativeLayout;
    @BindView(R.id.rtv_started_time) RelativeTimeTextView startedTime;

    @BindView(R.id.iv_qr_code) ImageView ivQRCode;

    Calendar calendar = Calendar.getInstance();
    long classEndTime;
    Boolean flag = true;
    User currUser;

    ArrayAdapter<String> adapter;
    DatabaseReference dbRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_class);
        ButterKnife.bind(this);

        currUser = Paper.book().read(Constants.CURR_USER_KEY);
        dbRef = FirebaseDatabase.getInstance().getReference();

        String[] semesters = getResources().getStringArray(R.array.semesters);
        adapter = new ArrayAdapter<String>(CreateClassActivity.this, android.R.layout.simple_list_item_1, semesters);
        spSemesters.setAdapter(adapter);

        spSemesters.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    String[] firstSemesterSubs = getResources().getStringArray(R.array.semester_1_subs);
                    adapter = new ArrayAdapter<String>(CreateClassActivity.this, android.R.layout.simple_list_item_1, firstSemesterSubs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 1) {
                    String[] secSubs = getResources().getStringArray(R.array.semester_2_subs);
                    adapter = new ArrayAdapter<String>(CreateClassActivity.this, android.R.layout.simple_list_item_1, secSubs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 2) {
                    String[] subs = getResources().getStringArray(R.array.semester_3_subs);
                    adapter = new ArrayAdapter<String>(CreateClassActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 3) {
                    String[] subs = getResources().getStringArray(R.array.semester_4_subs);
                    adapter = new ArrayAdapter<String>(CreateClassActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 4) {
                    String[] subs = getResources().getStringArray(R.array.semester_5_subs);
                    adapter = new ArrayAdapter<String>(CreateClassActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 5) {
                    String[] subs = getResources().getStringArray(R.array.semester_6_subs);
                    adapter = new ArrayAdapter<String>(CreateClassActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 6) {
                    String[] subs = getResources().getStringArray(R.array.semester_7_subs);
                    adapter = new ArrayAdapter<String>(CreateClassActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 7) {
                    String[] subs = getResources().getStringArray(R.array.semester_8_subs);
                    adapter = new ArrayAdapter<String>(CreateClassActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @OnClick(R.id.tiet_duration)
    public void setDuration() {
        TimePickerDialog timePickerDialog = TimePickerDialog
                .newInstance(new TimePickerDialog.OnTimeSetListener() {
                                 @Override
                                 public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                                     Calendar datetime = Calendar.getInstance();
                                     Calendar c = Calendar.getInstance();
                                     datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                     datetime.set(Calendar.MINUTE, minute);

//                                     etDuration.setText(hourOfDay + ":" + minute);
//                                     classEndTime = datetime.getTimeInMillis();

//                                     if (datetime.getTimeInMillis() >= c.getTimeInMillis()) {
//                                         Log.d("ClassDuration: ", "duration is in future");
//                                     }
                                    if (datetime.getTimeInMillis() >= c.getTimeInMillis()) {
                                        //it's after current
                                        Log.d("IRFAN", String.valueOf(datetime.getTimeInMillis()));

                                        int hour = hourOfDay % 12;
                                        etDuration.setText(String.format("%02d : %02d %s", hour == 0 ? 12 : hour,
                                                minute, hourOfDay < 12 ? "AM" : "PM"));

//                                        etDuration.setText(hourOfDay +":"+ minute);
                                        classEndTime = datetime.getTimeInMillis();


                                    } else {
                                        //it's before current'
                                        etDuration.setError("Invalid Time");
                                        flag = false;
                                    }
                                 }

                             },

                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        false
                );

        Calendar currTime = Calendar.getInstance();
        timePickerDialog.setMinTime(currTime.get(Calendar.HOUR_OF_DAY), currTime.get(Calendar.MINUTE), currTime.get(Calendar.SECOND));
        timePickerDialog.show(getFragmentManager(), "TimePickerDialog");
    }


    @OnClick(R.id.btn_generate_qr_code)
    public void setBtnGenerate() {
        boolean isDuration = true;
        if (etDuration.getText().toString().isEmpty()) {
            etDuration.setError("Duration required!");
            isDuration = false;
        }

        if (!isDuration) {
            return;
        }

        String subject = spSubjects.getSelectedItem().toString();

        Date date = new Date();
        long time = date.getTime();

        Calendar calendar = Calendar.getInstance();
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        String monthNameWithDate = Helper.getMonthName(month) + "_" + dayOfMonth;

        ClassInfo info = new ClassInfo();
        info.instructorUID = currUser.id;
        info.semesterID = Helper.getSemesterId(CreateClassActivity.this, spSemesters.getSelectedItem().toString());
        info.dateTime = calendar.getTimeInMillis();
        info.subject = subject;
        info.duration = classEndTime;

        Gson gson = new Gson();
        String classInfo = gson.toJson(info);

        linearLayout.setVisibility(View.GONE);

        relativeLayout.setVisibility(View.VISIBLE);
        MultiFormatWriter writer = new MultiFormatWriter();

        try {
            BitMatrix matrix = writer.encode(classInfo, BarcodeFormat.QR_CODE, 1000, 1000);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap = encoder.createBitmap(matrix);
            ivQRCode.setImageBitmap(bitmap);
            startedTime.setReferenceTime(calendar.getTimeInMillis());

            String courseId = Helper.getCourseId(this, info.subject);

            Subject sub = new Subject();
            sub.id = courseId;
            sub.name = info.subject;
            sub.date = monthNameWithDate;
            sub.year = calendar.get(Calendar.YEAR);

            dbRef.child("attendance")
                    .child(String.valueOf(calendar.get(Calendar.YEAR)))
                    .child(monthNameWithDate)
                    .child(info.instructorUID)
                    .child(courseId)
                    .setValue(sub);

            String semesterId = Helper.getSemesterId(this, spSemesters.getSelectedItem().toString());

            dbRef.child("today")
                    .child(monthNameWithDate)
                    .child(semesterId)
                    .child(courseId)
                    .setValue(sub);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
