package com.example.irfan.smartattendy;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class SignInActivity extends AppCompatActivity {


    @BindView(R.id.text_input_phone_number) TextInputEditText etPhoneNumber;
    @BindView(R.id.text_input_password) TextInputEditText etPassword;
    @BindView(R.id.tv_sign_up) TextView tvSignUp;
    @BindView(R.id.btn_sign_in) Button btnSignIn;
//    @BindView(R.id.iv_arrow_up) ImageView ivArrowUp;
//    @BindView(R.id.tv_link) TextView tvLink;

    FirebaseAuth auth;
    DatabaseReference dbRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        auth = FirebaseAuth.getInstance();
        dbRef = FirebaseDatabase.getInstance().getReference();
    }

    @OnClick(R.id.btn_sign_in)
    public void setSignIn() {
        boolean flag = true;

        if (etPhoneNumber.getText().toString().isEmpty()) {
            etPhoneNumber.setError("Phone Number Required!");
            flag = false;

        } else {
            etPhoneNumber.setError(null);
        }

        if (etPassword.getText().toString().isEmpty()) {
            etPassword.setError("Password Required!");
            flag = false;

        } else {
            etPassword.setError(null);
        }

        if (!flag) {
            return;
        }

        Helper.showProgress(this, true);
        auth.signInWithEmailAndPassword(etPhoneNumber.getText().toString().trim(), etPassword.getText().toString().trim())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Helper.removeProgress(SignInActivity.this);
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            Log.d("TAG", firebaseUser.getUid());

                            if (auth.getCurrentUser().isEmailVerified()) {
                                getUserData(firebaseUser.getUid());

                            } else {
                                startActivity(new Intent(SignInActivity.this, EmailVerificationSentActivity.class));
                                finish();
                                Bungee.slideLeft(SignInActivity.this);
                            }

                        } else {
                            Helper.removeProgress(SignInActivity.this);
                            Toast.makeText(SignInActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }

    public void getUserData(String userId) {
        dbRef.child("users").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                Paper.book().write(Constants.CURR_USER_KEY, user);

                startActivity(new Intent(SignInActivity.this, HomeActivity.class));
                finish();
                Bungee.slideLeft(SignInActivity.this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

//    @OnClick(R.id.iv_arrow_up)
//    public void setArrowUp() {
//        Bungee.slideUp(this);
//        startActivity(new Intent(this, SignUpActivity.class));
//    }

    @OnClick(R.id.tv_sign_up)
    public void setLink() {
        startActivity(new Intent(SignInActivity.this, UserRoleActivity.class));
        Bungee.slideUp(this);
    }
}
