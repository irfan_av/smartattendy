package com.example.irfan.smartattendy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

public class EmailVerificationSentActivity extends AppCompatActivity {

    @BindView(R.id.tv_not_received_mail) TextView tvNotReceivedMail;
    @BindView(R.id.btn_verified) Button btnVerified;

    FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification_sent);
        ButterKnife.bind(this);

        auth = FirebaseAuth.getInstance();

        tvNotReceivedMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (auth.getCurrentUser().isEmailVerified()) {
                    Toast.makeText(EmailVerificationSentActivity.this, "You have already verified your email", Toast.LENGTH_LONG).show();

                } else {
                    auth.getCurrentUser().sendEmailVerification();
                    Toast.makeText(EmailVerificationSentActivity.this, "Email sent, please check your inbox", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    @OnClick(R.id.btn_verified)
    public void setBtnVerified() {
        Intent intent = new Intent(EmailVerificationSentActivity.this, SignInActivity.class);
        startActivity(intent);
        finish();
        Bungee.slideRight(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
