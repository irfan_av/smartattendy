package com.example.irfan.smartattendy;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.irfan.smartattendy.adapters.SubjectListAdapter;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Subject;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class SubjectListActivity extends AppCompatActivity {

    @BindView(R.id.tv_label_subjects) TextView textView;
    @BindView(R.id.tv_date) TextView tvDate;
    @BindView(R.id.lv_subjects_list) ListView lvSubjectList;
    @BindView(R.id.tv_no_record) TextView tvNoRecord;

    DatabaseReference dbRef;
    List<Subject> subjects;
    SubjectListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_list);
        ButterKnife.bind(this);

        dbRef = FirebaseDatabase.getInstance().getReference();
        User currUser = Paper.book().read(Constants.CURR_USER_KEY);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle("Classes");
            bar.setDisplayShowHomeEnabled(true);
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.toolbar_drawable));
        }

        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth =  calendar.get(Calendar.DAY_OF_MONTH);
        String monthNameWithDate = Helper.getMonthName(month) + "_" + dayOfMonth;


        subjects = new ArrayList<>();
        adapter = new SubjectListAdapter(this, subjects);
        lvSubjectList.setAdapter(adapter);

//        if (adapter.getCount() == 0) {
//            tvNoRecord.setVisibility(View.VISIBLE);
//
//        } else {
//            tvNoRecord.setVisibility(View.GONE);
//        }

        String monthDate = getIntent().getStringExtra("DATE");
        String year = getIntent().getStringExtra("YEAR");

        DatabaseReference userRef;
        if (monthDate != null && year != null) {
            userRef = dbRef.child("attendance").child(year).child(monthDate).child(currUser.id);

            String monthName = monthDate.substring(0, Math.min(monthDate.length(), 3));
            String date = monthDate.substring(monthDate.length() - 2);

            textView.setVisibility(View.GONE);
            tvDate.setVisibility(View.VISIBLE);

            if (! String.valueOf(calendar.get(Calendar.YEAR)).equals(year)) {
                tvDate.setText(checkUnderscore(date) + " " + monthName + ", " + year );

            } else {
                tvDate.setText(checkUnderscore(date) + " " + monthName);
            }

        } else {
            userRef = dbRef.child("attendance").child(String.valueOf(calendar.get(Calendar.YEAR)))
                    .child(monthNameWithDate).child(currUser.id);

            textView.setVisibility(View.VISIBLE);
            tvDate.setVisibility(View.GONE);

        }

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subjects.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Subject subject = snapshot.getValue(Subject.class);
                    subjects.add(subject);
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public String checkUnderscore(String date) {
        if (date.substring(0, Math.min(date.length(), 1)).equals("_")) {
            return date.substring(date.length() - 1);

        } else {
            return date;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                Bungee.slideRight(this);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);

    }
}
