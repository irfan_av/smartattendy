package com.example.irfan.smartattendy.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.irfan.smartattendy.R;
import com.example.irfan.smartattendy.models.User;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by irfan on 11/24/18.
 */

public class UserListAdapter extends ArrayAdapter {

    private Context context;
    private List<User> users;

    public UserListAdapter(@NonNull Context context, List<User> users) {
        super(context, R.layout.attendee_list_item, users);

        this.context = context;
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = View.inflate(context, R.layout.attendee_list_item, null);

        CircleImageView civUserAvatar = view.findViewById(R.id.civ_user_avatar);
        TextView tvUserName = view.findViewById(R.id.tv_user_name);

        User user = users.get(position);
        tvUserName.setText(user.name);

        if (user.photoUrl != null) {
            Glide.with(context.getApplicationContext()).load(user.photoUrl).into(civUserAvatar);

        } else {
            civUserAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.avatar));
        }
        return view;
    }
}
