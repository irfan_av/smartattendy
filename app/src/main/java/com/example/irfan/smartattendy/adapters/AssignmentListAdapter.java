package com.example.irfan.smartattendy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.irfan.smartattendy.AssignmentViewActivity;
import com.example.irfan.smartattendy.R;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Assignment;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import spencerstudios.com.bungeelib.Bungee;

/**
 * Created by irfan on 12/26/18.
 */

public class AssignmentListAdapter extends ArrayAdapter {

    Context context;
    List<Assignment> assignments;

    public AssignmentListAdapter(@NonNull Context context, List<Assignment> assignments) {
        super(context, R.layout.assignment_list_item, assignments);
        this.context = context;
        this.assignments = assignments;
    }

    @Override
    public int getCount() {
        return assignments.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = View.inflate(context, R.layout.assignment_list_item, null);

        TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvCourse = view.findViewById(R.id.tv_course);

        Assignment assignment = assignments.get(position);
        tvTitle.setText(assignment.title);
        tvCourse.setText(assignment.course);

        view.setOnClickListener(view1 -> {
            Intent intent = new Intent(context.getApplicationContext(), AssignmentViewActivity.class);
            intent.putExtra("ASSIGNMENT", assignment);
            context.startActivity(intent);
            Bungee.slideLeft(context);
        });

        return view;
    }
}
