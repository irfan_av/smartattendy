package com.example.irfan.smartattendy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Space;

import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.auth.FirebaseAuth;

import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class SplashActivity extends AppCompatActivity {

    User currUser;

    FirebaseAuth auth = FirebaseAuth.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        currUser = Paper.book().read(Constants.CURR_USER_KEY);

        LinearLayout linearLayout = findViewById(R.id.linear_layout);
        linearLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (currUser != null && auth.getCurrentUser().isEmailVerified()) {
                    intent = new Intent(SplashActivity.this, HomeActivity.class);

                } else {
                    intent = new Intent(SplashActivity.this, SignInActivity.class);
                }

                startActivity(intent);
                finish();
                Bungee.fade(SplashActivity.this);
            }
        }, 1500);
    }
}
