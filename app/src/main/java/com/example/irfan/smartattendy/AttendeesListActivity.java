package com.example.irfan.smartattendy;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.irfan.smartattendy.adapters.AttendeeListAdapter;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class AttendeesListActivity extends AppCompatActivity {

    @BindView(R.id.tv_label_attendees_list) TextView textView;

    DatabaseReference dbRef;
    User currUser;

    List<User> users;
    AttendeeListAdapter adapter;
    Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendees_list);
        ButterKnife.bind(this);

        String courseId = getIntent().getStringExtra("COURSEID");
        String date = getIntent().getStringExtra("DATE");
//        String year = getIntent().getStringExtra("YEAR");
        long year = getIntent().getLongExtra("YEAR", calendar.get(Calendar.YEAR));

        Log.d("TAG", year + date);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            if (courseId != null) {
                String course = Helper.getCourseName(this, courseId);
                bar.setTitle(course);
                textView.setText(course + " class's Attendance" );
            }
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.toolbar_drawable));
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        dbRef = FirebaseDatabase.getInstance().getReference();
        currUser = Paper.book().read(Constants.CURR_USER_KEY);

        ListView listView = findViewById(R.id.lv_attendees);

        users = new ArrayList<>();
        if (courseId != null) {
            adapter = new AttendeeListAdapter(this, users, courseId, year);
            listView.setAdapter(adapter);
        }

        if (courseId != null && date != null && year != 0) {
            dbRef.child("attendance")
                    .child(String.valueOf(year))
                    .child(date)
                    .child(currUser.id)
                    .child(courseId)
                    .child("attendees")
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            users.clear();

                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                String userId = snapshot.getKey();

                                Log.d("TAG", userId);
                                if (userId != null) {
                                    dbRef.child("users")
                                            .child(userId)
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            User user = dataSnapshot.getValue(User.class);
                                            users.add(user);
                                            adapter.notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                Bungee.slideRight(this);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);

    }
}
