package com.example.irfan.smartattendy.models;

import java.io.Serializable;

/**
 * Created by irfan on 12/26/18.
 */

public class Assignment implements Serializable{
    public String title;
    public String description;
    public String course;
    public String semester;
    public long dueDate;

}
