package com.example.irfan.smartattendy;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class HomeActivity extends NavDrawerActivity {

    @BindView(R.id.cv_create_qr_code) CardView cvCreateQRCode;
    @BindView(R.id.cv_see_list) CardView cvSeeList;
    @BindView(R.id.cv_create_assignment) CardView cvCreateAssignment;
    @BindView(R.id.cv_create_qr_scanner) CardView cvQRScanner;
    @BindView(R.id.cv_today_classes) CardView cvClass;
    @BindView(R.id.cv_previous_attendance) CardView cvPreviousAttendance;
    @BindView(R.id.cv_assignment_list) CardView cvAssignmentList;
    @BindView(R.id.cv_my_attendance) CardView cvMyAttendance;

    @BindView(R.id.ll_instructor) LinearLayout llInstructor;
    @BindView(R.id.ll2_instructor) LinearLayout ll2Instructor;

    @BindView(R.id.ll_student) LinearLayout llStudent;
    @BindView(R.id.ll2_student) LinearLayout ll2Student;

    User currUser;
    DatabaseReference dbRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_home);
        ButterKnife.bind(this);

        currUser = Paper.book().read(Constants.CURR_USER_KEY);
        dbRef = FirebaseDatabase.getInstance().getReference();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.toolbar_drawable));
            actionBar.setTitle("Home");
        }

    }
    
    @Override
    protected void onStart() {
        super.onStart();

        if (currUser != null) {
            if (currUser.roleId == 0) {
                llStudent.setVisibility(View.VISIBLE);
                ll2Student.setVisibility(View.VISIBLE);

                llInstructor.setVisibility(View.GONE);
                ll2Instructor.setVisibility(View.GONE);

                if (currUser.semester == null && currUser.gender == null && currUser.enrollmentNumber == null
                        && currUser.dob == 0) {

                    Intent intent = new Intent(this, UpdateProfileActivity.class);
                    intent.putExtra("STUDENT", "STUDENT");
                    startActivity(intent);
                    Bungee.slideLeft(this);
                }
            }

            else if (currUser.roleId == 1) {
                llStudent.setVisibility(View.GONE);
                ll2Student.setVisibility(View.GONE);

                llInstructor.setVisibility(View.VISIBLE);
                ll2Instructor.setVisibility(View.VISIBLE);

                if (currUser.gender == null && currUser.qualification == null) {
                    Intent intent = new Intent(this, UpdateProfileActivity.class);
                    intent.putExtra("INSTRUCTOR", "INSTRUCTOR");
                    startActivity(intent);
                    Bungee.slideLeft(this);
                }
            }

        } else {
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        }

    }


    @OnClick(R.id.cv_create_qr_code)
    public void setCvCreateQRCode() {
        startActivity(new Intent(HomeActivity.this, CreateClassActivity.class));
        Bungee.slideLeft(this);
    }


    @OnClick(R.id.cv_see_list)
    public void setCvSeeList() {
        startActivity(new Intent(this, SubjectListActivity.class));
        Bungee.slideLeft(this);
    }


    @OnClick(R.id.cv_create_qr_scanner)
    public void setCvQRScanner() {
        startActivity(new Intent(this, CodeScanActivity.class));
        Bungee.slideLeft(this);
    }


    @OnClick(R.id.cv_today_classes)
    public void setCvClass() {
        startActivity(new Intent(this, TodayClassesForStudentActivity.class));
        Bungee.slideLeft(this);
    }

    @OnClick(R.id.cv_create_assignment)
    public void setCvEditProfile() {
        startActivity(new Intent(this, CreateAssignmentActivity.class));
        Bungee.slideLeft(this);
    }

    @OnClick(R.id.cv_previous_attendance)
    public void setCvPreviousAttendance() {
        startActivity(new Intent(this, AttendanceActivity.class));
        Bungee.slideLeft(this);
    }

    @OnClick(R.id.cv_assignment_list)
    public void assignmentList() {
        startActivity(new Intent(this, AssignmentListActivity.class));
        Bungee.slideLeft(this);
    }

    @OnClick(R.id.cv_my_attendance)
    public void myAttendance() {
        startActivity(new Intent(this, MyAttendanceActivity.class));
        Bungee.slideLeft(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        Bungee.slideDown(this);
    }
}
