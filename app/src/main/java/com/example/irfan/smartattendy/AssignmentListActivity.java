package com.example.irfan.smartattendy;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.irfan.smartattendy.adapters.AssignmentListAdapter;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Assignment;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class AssignmentListActivity extends NavDrawerActivity {

    @BindView(R.id.lv_assignment) ListView lvAssignment;

    User currUser;
    DatabaseReference dbRef;

    List<Assignment> assignments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_list);
        ButterKnife.bind(this);


        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.toolbar_drawable));
            bar.setTitle("Assignments");
        }

        currUser = Paper.book().read(Constants.CURR_USER_KEY);
        dbRef = FirebaseDatabase.getInstance().getReference();
        assignments = new ArrayList<>();


        AssignmentListAdapter adapter = new AssignmentListAdapter(this, assignments);
        ListView listView = findViewById(R.id.lv_assignment);
        listView.setAdapter(adapter);

        String semesterCode = Helper.getSemesterId(this, currUser.semester);

        dbRef.child("assignments")
                .child(semesterCode)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                assignments.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Assignment assignment = snapshot.getValue(Assignment.class);
                    assignments.add(assignment);
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
