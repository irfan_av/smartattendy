package com.example.irfan.smartattendy.models;

import java.io.Serializable;

/**
 * Created by irfan on 11/21/18.
 */

public class User implements Serializable {
    public String id;
    public String photoUrl;
    public String name;
    public String fatherName;
    public String email;
    public String phone;
    public int roleId;
    public String gender;
    public String enrollmentNumber;
    public String admitCardNumber;
    public String classRollNo;
    public String semester;
    public String qualification;
    public long dob;

}
