package com.example.irfan.smartattendy;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Assignment;
import com.example.irfan.smartattendy.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class CreateAssignmentActivity extends AppCompatActivity {

    @BindView(R.id.sp_semester) Spinner spSemesters;
    @BindView(R.id.sp_subjects) Spinner spSubjects;
    @BindView(R.id.et_description) TextInputEditText etDescription;
    @BindView(R.id.et_title) TextInputEditText etTitle;
    @BindView(R.id.et_due_date) TextInputEditText etDueDate;
    @BindView(R.id.btn_post) Button btnPost;


    User currUser;
    DatabaseReference dbRef;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_assignment);
        ButterKnife.bind(this);

        currUser = Paper.book().read(Constants.CURR_USER_KEY);
        dbRef = FirebaseDatabase.getInstance().getReference();

        String[] semesters = getResources().getStringArray(R.array.semesters);
        adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, android.R.layout.simple_list_item_1, semesters);
        spSemesters.setAdapter(adapter);

        spSemesters.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    String[] firstSemesterSubs = getResources().getStringArray(R.array.semester_1_subs);
                    adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, android.R.layout.simple_list_item_1, firstSemesterSubs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 1) {
                    String[] secSubs = getResources().getStringArray(R.array.semester_2_subs);
                    adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, android.R.layout.simple_list_item_1, secSubs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 2) {
                    String[] subs = getResources().getStringArray(R.array.semester_3_subs);
                    adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 3) {
                    String[] subs = getResources().getStringArray(R.array.semester_4_subs);
                    adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 4) {
                    String[] subs = getResources().getStringArray(R.array.semester_5_subs);
                    adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 5) {
                    String[] subs = getResources().getStringArray(R.array.semester_6_subs);
                    adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 6) {
                    String[] subs = getResources().getStringArray(R.array.semester_7_subs);
                    adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                } else if (i == 7) {
                    String[] subs = getResources().getStringArray(R.array.semester_8_subs);
                    adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, android.R.layout.simple_list_item_1, subs);
                    spSubjects.setAdapter(adapter);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etDueDate.setText(Helper.formatDate(cal.getTime()));
            }
        };

        etDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                if (!etDueDate.getText().toString().isEmpty())
                    now.setTime(Helper.parseDate(etDueDate.getText().toString()));

                DatePickerDialog dpd = DatePickerDialog.newInstance(dateSetListener, now);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                dpd.setMinDate(calendar);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

    }

    @OnClick(R.id.btn_post)
    public void postAssignment() {

        boolean flag = true;

        if (etTitle.getText().toString().isEmpty()) {
            etTitle.setError("Required!");
            flag = false;
        }

        if (etDescription.getText().toString().isEmpty()) {
            etDescription.setError("Required!");
            flag = false;
        }

        if (etDueDate.getText().toString().isEmpty()) {
            etDueDate.setError("Required!");
            flag = false;
        }

        if (!flag) {
            return;
        }

        Assignment assignment = new Assignment();
        assignment.course = spSubjects.getSelectedItem().toString();
        assignment.semester = spSemesters.getSelectedItem().toString();
        assignment.description = etDescription.getText().toString();
        assignment.title = etTitle.getText().toString();
        assignment.dueDate = Helper.parseDate(etDueDate.getText().toString()).getTime();

        String courseId = Helper.getCourseId(this, spSubjects.getSelectedItem().toString());
        String semesterId = Helper.getSemesterId(this, spSemesters.getSelectedItem().toString());

        Helper.showProgress(this, true);
        dbRef.child("assignments")
                .child(semesterId)
                .child(courseId)
                .setValue(assignment)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Helper.removeProgress(CreateAssignmentActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);

    }
}
