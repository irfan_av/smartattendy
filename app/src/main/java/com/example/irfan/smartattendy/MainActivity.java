package com.example.irfan.smartattendy;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.models.ClassInfo;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.paperdb.Paper;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tv_user_name) TextView tvUserName;
    @BindView(R.id.iv_qr_code) ImageView ivQRCode;
    @BindView(R.id.et_subject) EditText etSubject;

    @BindView(R.id.btn_generate_qr_code) Button btnGenerateQR;
    @BindView(R.id.btn_scan) Button btnScan;
    @BindView(R.id.btn_see_list) Button btnSeeList;
    @BindView(R.id.btn_sign_out) Button btnSignOut;

    User currUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        currUser = Paper.book().read(Constants.CURR_USER_KEY);
        if (currUser != null) {
            tvUserName.setText(currUser.name);

        }


    }

    @OnClick(R.id.btn_generate_qr_code)
    public void generateQR() {

        ClassInfo info = new ClassInfo();
        info.startedAt = 123456789;
        info.subject = "English";
        info.courseId = "E301";
        info.instructorUID = "Irfan";

        Gson gson = new Gson();
        String json = gson.toJson(info);

//        String subject = etSubject.getText().toString();
//        if (!subject.isEmpty()) {
            MultiFormatWriter writer = new MultiFormatWriter();
            try {
                BitMatrix matrix = writer.encode(json, BarcodeFormat.QR_CODE, 800, 800);
                BarcodeEncoder encoder = new BarcodeEncoder();
                Bitmap bitmap = encoder.createBitmap(matrix);
                ivQRCode.setImageBitmap(bitmap);

            } catch (WriterException e) {
                e.printStackTrace();
            }
//        }

    }

    @OnClick(R.id.btn_scan)
    public void scanCode() {
        startActivity(new Intent(MainActivity.this, CodeScanActivity.class));
    }


    @OnClick(R.id.btn_see_list)
    public void seeList() {
        startActivity(new Intent(MainActivity.this, SubjectListActivity.class));
    }

    @OnClick(R.id.btn_sign_out)
    public void signOut() {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(MainActivity.this, SignInActivity.class));
        finish();
    }

}
