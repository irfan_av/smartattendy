package com.example.irfan.smartattendy;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.irfan.smartattendy.adapters.SubjectListAdapter;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Subject;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class TodayClassesForStudentActivity extends NavDrawerActivity {

    @BindView(R.id.lv_classes) ListView lvClasses;
    @BindView(R.id.tv_semester_not_specified) TextView textView;

    List<Subject> subjects;
    DatabaseReference dbRef;
    User currUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today_classes_for_student);

        ButterKnife.bind(this);

        dbRef = FirebaseDatabase.getInstance().getReference();
        currUser = Paper.book().read(Constants.CURR_USER_KEY);


        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle("Classes");
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.toolbar_drawable));
        }

        subjects = new ArrayList<>();
        SubjectListAdapter adapter = new SubjectListAdapter(this, subjects);

        if (currUser.semester != null) {
            textView.setVisibility(View.GONE);
            lvClasses.setVisibility(View.VISIBLE);

            String semesterId = Helper.getSemesterId(this, currUser.semester);

            Calendar calendar = Calendar.getInstance();
            int date = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            String monthName = Helper.getMonthName(month);

            dbRef.child("today")
                    .child(monthName + "_" + date)
                    .child(semesterId)
                    .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    subjects.clear();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Subject subject = snapshot.getValue(Subject.class);
                        subjects.add(subject);
                    }

                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            lvClasses.setAdapter(adapter);

        } else {
            textView.setVisibility(View.VISIBLE);
            lvClasses.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
