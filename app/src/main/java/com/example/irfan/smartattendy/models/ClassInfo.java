package com.example.irfan.smartattendy.models;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by irfan on 12/5/18.
 */

public class ClassInfo implements Serializable {

    public long startedAt;
    public String semesterID;
    public String subject;
    public String courseId;
    public long dateTime;
    public long duration;
    public String instructorUID;
}
