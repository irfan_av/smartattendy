package com.example.irfan.smartattendy.helper;

/**
 * Created by irfan on 12/4/18.
 */

public class Constants {

    public static final String USER_FRIENDLY_DATE_FORMAT = "yyyy-MM-dd";    //Date format in app

    public static final String CURR_USER_KEY = "CURR_USER_KEY";
}
