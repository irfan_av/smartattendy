package com.example.irfan.smartattendy;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.Course;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class StudentAttendanceActivity extends AppCompatActivity {

    @BindView(R.id.tv_overall) TextView tvOverAll;
    @BindView(R.id.tv_present) TextView tvPresent;
    @BindView(R.id.tv_absent) TextView tvAbsent;

    User currUser;
    DatabaseReference dbRef;
    Calendar calendar = Calendar.getInstance();

    int classesCounter = 0;
    int presentCounter = 0;
    int absentCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_attendance);
        ButterKnife.bind(this);

        Course course = (Course) getIntent().getSerializableExtra("COURSE");

        currUser = Paper.book().read(Constants.CURR_USER_KEY);
        dbRef = FirebaseDatabase.getInstance().getReference();

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            if (course != null) {
                bar.setTitle(course.name);
            }

            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.background_toolbar));
            bar.setDisplayShowHomeEnabled(true);
            bar.setDisplayHomeAsUpEnabled(true);
        }

        Helper.showProgress(this, true);

        dbRef.child("attendance")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            String year = dataSnapshot1.getKey();
                            if (year != null)
                            dbRef.child("attendance")
                                    .child(year)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                String date = snapshot.getKey();

                                                if (date != null) {
                                                    dbRef.child("attendance")
                                                            .child(year)
                                                            .child(date)
                                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                    for (DataSnapshot snapshot1 : dataSnapshot.getChildren()) {
                                                                        String uid = snapshot1.getKey();

                                                                        if (uid != null) {
                                                                            dbRef.child("attendance")
                                                                                    .child(year)
                                                                                    .child(date)
                                                                                    .child(uid)
                                                                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                        @Override
                                                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                                            for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                                                                                String courseId = snap.getKey();

                                                                                                if (course != null && courseId != null && courseId.equals(course.id)) {
                                                                                                    classesCounter++;

                                                                                                    dbRef.child("attendance")
                                                                                                            .child(year)
                                                                                                            .child(date)
                                                                                                            .child(uid)
                                                                                                            .child(courseId)
                                                                                                            .child("attendees")
                                                                                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                                                @Override
                                                                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                                                                    for (DataSnapshot snapshot2 : dataSnapshot.getChildren()) {
                                                                                                                        String id = snapshot2.getKey();

                                                                                                                        if (currUser.id.equals(id)) {
                                                                                                                            presentCounter++;
                                                                                                                        }
                                                                                                                    }

                                                                                                                    absentCounter = classesCounter - presentCounter;

                                                                                                                    tvOverAll.setText(String.valueOf(classesCounter));
                                                                                                                    tvPresent.setText(String.valueOf(presentCounter));
                                                                                                                    tvAbsent.setText(String.valueOf(absentCounter));
                                                                                                                }

                                                                                                                @Override
                                                                                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                                                                }
                                                                                                            });
                                                                                                }

                                                                                                Helper.removeProgress(StudentAttendanceActivity.this);

                                                                                            }
                                                                                        }

                                                                                        @Override
                                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                                        }
                                                                                    });
                                                                        }

                                                                        Helper.removeProgress(StudentAttendanceActivity.this);

                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                }
                                                            });
                                                }

                                                Helper.removeProgress(StudentAttendanceActivity.this);

                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                            Helper.removeProgress(StudentAttendanceActivity.this);

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                Bungee.slideRight(this);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
