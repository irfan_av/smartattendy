package com.example.irfan.smartattendy;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.User;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class UpdateProfileActivity extends AppCompatActivity {

    @BindView(R.id.civ_user_avatar) CircleImageView civUserAvatar;
    @BindView(R.id.tv_change_photo) TextView tvChangePhoto;
    @BindView(R.id.tiet_name) TextInputEditText etName;
    @BindView(R.id.tiet_father_name) TextInputEditText etFatherName;
    @BindView(R.id.tiet_email) TextInputEditText etEmail;
    @BindView(R.id.tiet_phone_number) TextInputEditText etPhoneNumber;
    @BindView(R.id.tiet_enrollment_num) TextInputEditText etEnrollmentNum;
    @BindView(R.id.til_qualification) TextInputLayout textInputLayout;
    @BindView(R.id.tiet_qualification) TextInputEditText etQualification;
    @BindView(R.id.tiet_dob) TextInputEditText etDob;
    @BindView(R.id.tiet_class_roll_no) TextInputEditText etClassRollNo;
    @BindView(R.id.tiet_admit_card) TextInputEditText etAdmitCard;
    @BindView(R.id.sp_semester) AppCompatSpinner spSemester;
    @BindView(R.id.sp_gender) AppCompatSpinner spGender;

    @BindView(R.id.ll_student_sec) LinearLayout llStudentSec;

    User currUser;
    StorageReference storageRef;
    StorageReference userPhotoRef;
    DatabaseReference dbRef;

    public static String imagePath;
    ActionBar bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        ButterKnife.bind(this);

        bar = getSupportActionBar();

        if (bar != null) {
//            bar.setElevation(0);
//            bar.setTitle("Update Profile");
            bar.setTitle(Html.fromHtml("<font color='#FA558E'>Update Profile</font>"));
//            bar.setDisplayShowHomeEnabled(true);
//            bar.setDisplayShowTitleEnabled(false);
//            bar.setDisplayHomeAsUpEnabled(true);
            bar.setBackgroundDrawable(getResources().getDrawable(android.R.color.transparent));
        }

        storageRef = FirebaseStorage.getInstance().getReference();
        dbRef = FirebaseDatabase.getInstance().getReference();

        updateUserData();

        if (currUser.roleId != 0) {
            llStudentSec.setVisibility(View.GONE);
            textInputLayout.setVisibility(View.VISIBLE);

        } else {
            llStudentSec.setVisibility(View.VISIBLE);
            textInputLayout.setVisibility(View.GONE);
        }

        tvChangePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);//one can be replaced with any action code

            }
        });

        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etDob.setText(Helper.formatDate(cal.getTime()));
            }
        };

        etDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                if (!etDob.getText().toString().isEmpty())
                    now.setTime(Helper.parseDate(etDob.getText().toString()));

                DatePickerDialog dpd = DatePickerDialog.newInstance(dateSetListener, now);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                dpd.setMaxDate(calendar);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });

    }

    public void updateUserData() {
        currUser = Paper.book().read(Constants.CURR_USER_KEY);

        etName.setText(currUser.name);
        etEmail.setText(currUser.email);
        etPhoneNumber.setText(currUser.phone);

        if (currUser.fatherName != null) {
            etFatherName.setText(currUser.fatherName);
        }

        if (currUser.gender != null) {
            spGender.setSelection(getIndex(spGender, currUser.gender));
        }

        if (currUser.roleId == 0) {
            if (currUser.dob != 0) {
                Date date = new Date(currUser.dob);
                etDob.setText(Helper.formatDate(date));
            }

            if (currUser.enrollmentNumber != null) {
                etEnrollmentNum.setText(currUser.enrollmentNumber);
            }

            if (currUser.semester != null) {
                spSemester.setSelection(getIndex(spSemester, currUser.semester));
            }

            if (currUser.classRollNo != null) {
                etClassRollNo.setText(currUser.classRollNo);
            }

            if (currUser.admitCardNumber != null) {
                etAdmitCard.setText(currUser.admitCardNumber);
            }

        } else if (currUser.roleId == 1){
            if (currUser.qualification != null) {
                etQualification.setText(currUser.qualification);
            }
        }

        if (currUser.photoUrl != null) {
            Glide.with(this).load(currUser.photoUrl).into(civUserAvatar);

        } else {
            civUserAvatar.setImageDrawable(Helper.setAvatar(currUser.gender, currUser.roleId,
                    UpdateProfileActivity.this));
        }
    }

    private int getIndex(Spinner spinner, String s){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(s)){
                return i;
            }
        }

        return 0;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    civUserAvatar.setImageURI(selectedImage);

                    civUserAvatar.setDrawingCacheEnabled(true);
                    civUserAvatar.buildDrawingCache();

                    Bitmap bitmap = ((BitmapDrawable) civUserAvatar.getDrawable()).getBitmap();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();

//                    imagePath = "users/" + UUID.randomUUID() + ".jpg";
                    imagePath = "users/"+currUser.id+".jpg";

                    userPhotoRef = storageRef.child(imagePath);

                    StorageMetadata metadata = new StorageMetadata.Builder().setContentType("image/jpg").build();

                    Helper.showProgress(this, true);
                    bar.hide();
                    UploadTask uploadTask = userPhotoRef.putBytes(data, metadata);

                    Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }

                            // Continue with the task to get the download URL
                            return userPhotoRef.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri downloadUri = task.getResult();
                                currUser.photoUrl = downloadUri.toString();

                            } else {
                                Log.d("TAG", "Error while uploading photo");
                                // Handle failures
                                // ...
                            }
                            Helper.removeProgress(UpdateProfileActivity.this);
                            bar.show();
                        }
                    });

//                    uploadTask.addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception exception) {
//                            // Handle unsuccessful uploads
//                            Toast.makeText(UpdateProfileActivity.this, "Image not uploaded", Toast.LENGTH_SHORT).show();
//                        }
//                    }).addOnSuccessListener(UpdateProfileActivity.this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                            Helper.removeProgress(UpdateProfileActivity.this);
//                        }
//                    });

                }

                break;
        }
    }

    public void saveProfile() {

        boolean flag = true;

        currUser.gender = spGender.getSelectedItem().toString();

        if (etFatherName.getText().toString().isEmpty()) {
            flag = false;
            etFatherName.setError("Required");

        } else {
            currUser.fatherName = etFatherName.getText().toString();
        }

        if (currUser.roleId == 0) {


            currUser.semester = spSemester.getSelectedItem().toString();
            if (etEnrollmentNum.getText().toString().isEmpty()) {
                flag = false;
                etEnrollmentNum.setError("Required!");
            }
            else if (etDob.getText().toString().isEmpty()) {
                flag = false;
                etDob.setError("Date of Birth Required!");
            }

            else {
                currUser.dob = Helper.parseDate(etDob.getText().toString()).getTime();
                currUser.enrollmentNumber = etEnrollmentNum.getText().toString();
            }

            if (!etClassRollNo.getText().toString().isEmpty()) {
                currUser.classRollNo = etClassRollNo.getText().toString().trim();
            }

            if (!etAdmitCard.getText().toString().isEmpty()) {
                currUser.admitCardNumber = etAdmitCard.getText().toString().trim();
            }

        } else {
            if (etQualification.getText().toString().isEmpty()) {
                flag = false;
                etQualification.setError("Qualification Required!");
            }
            else {
                currUser.qualification = etQualification.getText().toString();

            }
        }

        if (!flag) {
            return;
        }

        Helper.showProgress(this, true);
        bar.hide();
        Paper.book().write(Constants.CURR_USER_KEY, currUser);
        dbRef.child("users").child(currUser.id).setValue(currUser)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Helper.removeProgress(UpdateProfileActivity.this);
                bar.show();
            }
        });

//        userPhotoRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//            @Override
//            public void onSuccess(Uri uri) {
//                // Got the download URL for 'users/me/profile.png'
//                Helper.removeProgress(UpdateProfileActivity.this);
//                dbRef.child("users").child(currUser.id).child("photoUrl").setValue(uri.toString());
//
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception exception) {
//                // Handle any errors
//            }
//        });

    }

    @OnClick(R.id.tiet_email)
    public void setEmail() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Email Address")
                .setMessage("You can not change your email address")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void showDialog(String title, String message) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);

        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(title)
                .setMessage("You must have to provide "+ message +" otherwise you cannot proceed.\nAre you sure you want to leave?")
                .setPositiveButton("Leave", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        UpdateProfileActivity.super.onBackPressed();
                        FirebaseAuth.getInstance().signOut();
                        Paper.book().delete(Constants.CURR_USER_KEY);
                        Intent intent = new Intent(UpdateProfileActivity.this, SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        Bungee.slideRight(UpdateProfileActivity.this);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {}

                }).show();
    }

    @Override
    public void onBackPressed() {

        switch (currUser.roleId) {
            case 0:

                if (currUser.enrollmentNumber == null) {
                    showDialog("Enrollment Number Required", "Enrollment Number");
                    break;
                }

                else if (currUser.dob == 0) {
                    showDialog("Date of Birth Required", "Date of Birth");
                    break;
                }
//                    Intent intent = new Intent(UpdateProfileActivity.this, SignInActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                    finish();
//                    Bungee.slideRight(this);

                else {
                    super.onBackPressed();
                    Intent intent = new Intent(UpdateProfileActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Bungee.slideRight(this);
                    break;
                }
            case 1:
                if (currUser.qualification == null) {
                    showDialog("Qualification Required", "Qualification");
                    Log.d("TAG", "Qualification");
                    break;

//                    Intent intent = new Intent(UpdateProfileActivity.this, SignInActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                    finish();
//                    Bungee.slideRight(this);

                } else {
                    super.onBackPressed();
                    Intent intent = new Intent(UpdateProfileActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Bungee.slideRight(this);
                    break;
                }

            default:
                super.onBackPressed();
                Intent intent = new Intent(UpdateProfileActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                Bungee.zoom(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_update, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_item_save:
                saveProfile();
                startActivity(new Intent(UpdateProfileActivity.this, HomeActivity.class));
                finish();
                Bungee.slideLeft(this);
                break;

            case android.R.id.home:
                Bungee.slideRight(this);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);

    }
}
