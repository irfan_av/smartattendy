package com.example.irfan.smartattendy;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.User;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

public class MyProfileActivity extends AppCompatActivity {

    @BindView(R.id.tv_user_name) TextView tvUserName;
    @BindView(R.id.tv_father_name) TextView tvFatherName;
    @BindView(R.id.tv_user_phone) TextView tvPhone;
    @BindView(R.id.tv_user_dob) TextView tvDob;
    @BindView(R.id.tv_user_gender) TextView tvGender;
    @BindView(R.id.tv_user_email) TextView tvEmail;

    @BindView(R.id.student_table_layout) TableLayout llStudentSec;
    @BindView(R.id.tr_instructor_sec) TableRow tableRowInstructor;
    @BindView(R.id.tv_user_qualification) TextView tvQualification;
    @BindView(R.id.tv_user_semester) TextView tvSemester;
    @BindView(R.id.tv_user_class_roll_no) TextView tvClassRollNumber;
    @BindView(R.id.tv_admit_card_no) TextView tvAdmitCardNo;
    @BindView(R.id.tv_enrollment_no) TextView tvEnrollmentNo;

    User currUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);

        currUser = Paper.book().read(Constants.CURR_USER_KEY);

        CollapsingToolbarLayout toolbarLayout = findViewById(R.id.collapsing_toolbar);
        if (currUser.roleId == 1) {
            toolbarLayout.setTitle("Mr. " + currUser.name);

        } else {
            toolbarLayout.setTitle(currUser.name);
        }

        toolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
        toolbarLayout.setContentScrim(getResources().getDrawable(R.drawable.toolbar_drawable));

        ImageView imageView = findViewById(R.id.iv_image);

        if (currUser.photoUrl != null) {
            Glide.with(this)
                    .load(currUser.photoUrl)
                    .apply(new RequestOptions().centerCrop().placeholder(getResources().getDrawable(R.drawable.nav_header)))
                    .into(imageView);
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(currUser.name);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (currUser.dob != 0) {
            Date date = new Date(currUser.dob);
            tvDob.setText(Helper.formatDate(date));
        }

        else if (currUser.admitCardNumber != null) {
            tvAdmitCardNo.setText(currUser.admitCardNumber);
        }

        else if (currUser.classRollNo != null) {
            tvClassRollNumber.setText(currUser.classRollNo);
        }

        else {
            tvDob.setText("N/A");
        }

        tvUserName.setText(setData(currUser.name));
        tvFatherName.setText(setData(currUser.fatherName));
        tvEmail.setText(setData(currUser.email));
        tvSemester.setText(setData(currUser.semester));
        tvPhone.setText(setData(currUser.phone));
        tvQualification.setText(setData(currUser.qualification));
        tvClassRollNumber.setText(setData(currUser.classRollNo));
        tvEnrollmentNo.setText(setData(currUser.enrollmentNumber));
        tvAdmitCardNo.setText(setData(currUser.admitCardNumber));
        tvGender.setText(setData(currUser.gender));
    }

    public String setData(String data) {
        if (data != null) {
            return data;

        } else {
            return "N/A";
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (currUser.roleId == 0) {
            llStudentSec.setVisibility(View.VISIBLE);
            tableRowInstructor.setVisibility(View.GONE);

        } else {
            llStudentSec.setVisibility(View.GONE);
            tableRowInstructor.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d("TAG", "Home pressed");
                finish();
                Bungee.slideRight(this);
                break;

            case R.id.menu_item_edit:
                Log.d("TAG", "Edit icon pressed");
                startActivity(new Intent(this, UpdateProfileActivity.class));
                Bungee.slideLeft(this);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
