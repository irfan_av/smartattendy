package com.example.irfan.smartattendy.helper;

import android.app.Application;

import io.paperdb.Paper;

/**
 * Created by irfan on 11/21/18.
 */

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Paper.init(this);
    }
}
