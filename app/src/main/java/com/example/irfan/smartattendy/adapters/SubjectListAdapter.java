package com.example.irfan.smartattendy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.irfan.smartattendy.AttendeesListActivity;
import com.example.irfan.smartattendy.R;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.models.Subject;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

/**
 * Created by irfan on 12/10/18.
 */

public class SubjectListAdapter extends ArrayAdapter {

    private Context context;
    private List<Subject> subjects;

    public SubjectListAdapter(@NonNull Context context, List<Subject> subjects) {
        super(context, R.layout.subject_list_item, subjects);

        this.context = context;
        this.subjects = subjects;
    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = View.inflate(context, R.layout.subject_list_item, null);

        User currUser = Paper.book().read(Constants.CURR_USER_KEY);

        TextView textView = view.findViewById(R.id.tv_subject_name);

        Subject subject = subjects.get(position);
        textView.setText(subject.name);


        if (currUser.roleId != 0) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context.getApplicationContext(), AttendeesListActivity.class);
                    intent.putExtra("COURSEID", subject.id);
                    intent.putExtra("DATE", subject.date);
//                    intent.putExtra("YEAR", subject.year);
                    intent.putExtra("YEAR", subject.year);
                    context.startActivity(intent);
                    Bungee.slideLeft(context);
                }
            });

        }

        return view;
    }

}
