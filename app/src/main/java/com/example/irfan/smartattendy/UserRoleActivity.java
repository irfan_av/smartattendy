package com.example.irfan.smartattendy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

public class UserRoleActivity extends AppCompatActivity {

    @BindView(R.id.btn_student) TextView btnStudent;
    @BindView(R.id.btn_instructor) TextView btnInstructor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_role);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.btn_student)
    public void setBtnStudent() {
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.putExtra("ROLE", 0);
        startActivity(intent);
        finish();
        Bungee.zoom(this);
    }

    @OnClick(R.id.btn_instructor)
    public void setBtnInstructor() {
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.putExtra("ROLE", 1);
        startActivity(intent);
        finish();
        Bungee.zoom(this);
    }

}
