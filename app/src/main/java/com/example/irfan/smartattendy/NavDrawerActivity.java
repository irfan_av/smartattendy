package com.example.irfan.smartattendy;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.irfan.smartattendy.helper.Constants;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.auth.FirebaseAuth;

import de.hdodenhof.circleimageview.CircleImageView;
import io.paperdb.Paper;
import spencerstudios.com.bungeelib.Bungee;

/**
 * Created by irfan on 12/12/18.
 */

public class NavDrawerActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener {

    private FrameLayout view_stub;
    private NavigationView navigation_view;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Menu drawerMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_nav_drawer);
        view_stub = (FrameLayout) findViewById(R.id.view_stub);
        navigation_view = (NavigationView) findViewById(R.id.navigation_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, 0, 0);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
//        mDrawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.actionBarText));

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        drawerMenu = navigation_view.getMenu();
        for(int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i).setOnMenuItemClickListener(this);
        }

        setUserData();
    }

    public void setUserData() {
        View view = navigation_view.inflateHeaderView(R.layout.nav_header);
        TextView tvUserName = view.findViewById(R.id.tv_user_name);
        TextView tvUserRole = view.findViewById(R.id.tv_user_role);
        CircleImageView civUserPhoto = view.findViewById(R.id.civ_user_avatar);

        User currUser = Paper.book().read(Constants.CURR_USER_KEY);
        if (currUser != null) {
            tvUserName.setText(currUser.name);

            if (currUser.roleId == 0) {
                tvUserRole.setText("Student");

            } else {
                tvUserRole.setText("Instructor");
            }

            if (currUser.photoUrl != null) {
                Glide.with(this).load(currUser.photoUrl).into(civUserPhoto);

            } else {
                civUserPhoto.setImageDrawable(Helper.setAvatar(currUser.gender, currUser.roleId, NavDrawerActivity.this));
            }
        } else {
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /* Override all setContentView methods to put the content view to the FrameLayout view_stub
     * so that, we can make other activity implementations looks like normal activity subclasses.
     */
    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        getMenuInflater().inflate(R.menu.menu_item_indox, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

//        switch (item.getItemId()) {
//            case R.id.item_open_inbox:
//                startActivity(new Intent(NavDrawerActivity.this, MessageCenterActivity.class));
//                break;
//        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        Intent intent;
        // set item as selected to persist highlight
        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.nav_profile:
                mDrawerLayout.closeDrawers();
                startActivity(new Intent(NavDrawerActivity.this, MyProfileActivity.class));
                Bungee.slideLeft(this);
                break;

            case R.id.nav_settings:
                mDrawerLayout.closeDrawers();
                startActivity(new Intent(NavDrawerActivity.this, AboutActivity.class));
                Bungee.slideLeft(this);
                break;

            case R.id.nav_invite_friends:
                intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_app_msg));
                intent.setType("text/plain");
                startActivity(intent);
                break;

            case R.id.nav_sign_out:
                FirebaseAuth.getInstance().signOut();
                Paper.book().delete(Constants.CURR_USER_KEY);
                intent = new Intent(this, SignInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                Bungee.slideRight(this);
                finish();
        }

        return false;
    }
}


