package com.example.irfan.smartattendy;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.irfan.smartattendy.helper.Helper;
import com.example.irfan.smartattendy.models.ClassInfo;
import com.example.irfan.smartattendy.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AttendeeActivity extends AppCompatActivity {

    @BindView(R.id.civ_user_avatar) CircleImageView civUserAvatar;
    @BindView(R.id.tv_subject) TextView tvSubject;
    @BindView(R.id.tv_instructor) TextView tvInstructor;
    @BindView(R.id.tv_time) TextView tvTime;
    @BindView(R.id.tv_created_on) TextView tvClassStartedAt;
    @BindView(R.id.tv_scan_time) TextView tvScanTime;

    DatabaseReference dbRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendee);
        ButterKnife.bind(this);

        dbRef = FirebaseDatabase.getInstance().getReference();

        ClassInfo info = (ClassInfo) getIntent().getSerializableExtra("CLASSINFO");
        long classTime = getIntent().getLongExtra("CLASSTIME", 0);
        long scanTime = getIntent().getLongExtra("TIMESTAMP", 0);

        tvSubject.setText(info.subject);
        tvInstructor.setText(info.instructorUID);

        Calendar now = Calendar.getInstance();

        long remainingTime = info.duration - now.getTimeInMillis();

        CountDownTimer timer = new CountDownTimer(remainingTime, 1000) {
            @Override
            public void onTick(long l) {

                String text = String.format(Locale.getDefault(), "%02d : %02d : %02d ",
                        TimeUnit.MILLISECONDS.toHours(l) % 60,
                        TimeUnit.MILLISECONDS.toMinutes(l) % 60,
                        TimeUnit.MILLISECONDS.toSeconds(l) % 60);
                tvTime.setText(text);
            }

            @Override
            public void onFinish() {
                tvTime.setText("Time's up");
            }

        }.start();


        long scanRemainingTime =  scanTime - classTime;

        String text = String.format(Locale.getDefault(), "%02d: %02d: %02d",
                TimeUnit.MILLISECONDS.toHours(scanRemainingTime) % 60,
                TimeUnit.MILLISECONDS.toMinutes(scanRemainingTime) % 60,
                TimeUnit.MILLISECONDS.toSeconds(scanRemainingTime) % 60);

        if (scanRemainingTime < 1000*60) {
            tvScanTime.setText(Html.fromHtml("<font color='#00ff00'>Class just started now</font>"));

        } else {
            tvScanTime.setText(Html.fromHtml("You are  <font color='#FF0000'>"+ text +"</font>  late"));
        }


        dbRef.child("users").child(info.instructorUID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    if (user.gender != null) {
                        if (user.gender.equals("Male")) {
                            tvInstructor.setText("Mr. " + user.name);

                        } else {
                            tvInstructor.setText("Mrs. " + user.name);
                        }


                    } else {
                        tvInstructor.setText(user.name);
                    }

                    if (user.photoUrl != null) {
                        Glide.with(AttendeeActivity.this).load(user.photoUrl).into(civUserAvatar);
                        Log.d("TAG", "user photo: " + user.photoUrl);

                    } else {
                        civUserAvatar.setImageDrawable(Helper.setAvatar(user.gender, user.roleId, AttendeeActivity.this));
                        civUserAvatar.setElevation(0);
                        Log.d("TAG", "user photo: " + "Not available");

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        setClassStartTime(classTime);
    }

    public void  setClassStartTime(long createdOn) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(createdOn);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        int hour = hours % 12;
        tvClassStartedAt.setText(String.format("%02d:%02d %s", hour == 0 ? 12 : hour, minute,
                hours < 12 ? "AM" : "PM"));
    }
}
